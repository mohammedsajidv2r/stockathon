﻿using AutoMapper;
using SA_BaseObjet;
using SA_BaseObjet.Admin;
using SA_DA_Interface.Admin;
using SA_Mgr_Interface.Admin;
using SA_Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SA_Mgr.Admin
{
    public class UserOrderMgr : IUserOrderMgr
    {
        IUserOrderDA _IUserOrderDA;
        IMapper _mapper;

        public UserOrderMgr(IMapper IMapper, IUserOrderDA IUserOrderDA)
        {
            _IUserOrderDA = IUserOrderDA;
            _mapper = IMapper;
        }

        public ResultObject<string> userPlanUpdate(string RequestType, List<PlanOrderData> data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IUserOrderDA.userPlanUpdate(RequestType, data));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "userPlanUpdate", "IUserOrderMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<PlanOrderData>> viewPlanDetail(string RequestType, string param)
        {
            ResultObject<List<PlanOrderData>> resultObject = new ResultObject<List<PlanOrderData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<PlanOrderData>>>(_IUserOrderDA.viewPlanDetail(RequestType, param));
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewPlanDetail", "IUserOrderMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<PlanData>> viewPlan(string RequestType, string param1, string param2)
        {
            ResultObject<List<PlanData>> resultObject = new ResultObject<List<PlanData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<PlanData>>>(_IUserOrderDA.viewPlan(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewPlan", "IUserOrderMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> updatedetails(string RequestType, string Id, string xml, string param, string sp)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IUserOrderDA.updatedetails(RequestType, Id, xml, param, sp));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "userPlanUpdate", "IUserOrderMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<OrderData>> viewOrderDetailFilter(string RequestType, FilterData data)
        {
            ResultObject<List<OrderData>> resultObject = new ResultObject<List<OrderData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<OrderData>>>(_IUserOrderDA.viewOrderDetailFilter(RequestType, data));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewOrderDetailFilter", "IUserOrderMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
    }


}

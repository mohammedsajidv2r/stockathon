﻿using AutoMapper;
using SA_BaseObjet;
using SA_BaseObjet.Admin;
using SA_BaseObjet.StocksApi;
using SA_DA_Interface.Admin;
using SA_Mgr_Interface.Admin;
using SA_Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SA_Mgr.Admin
{
    public class AdminMgr : IAdminMgr
    {

        IAdminDA _IAdminDA;
        IMapper _mapper;

        public AdminMgr(IMapper IMapper, IAdminDA IAdminDA)
        {
            _IAdminDA = IAdminDA;
            _mapper = IMapper;
        }

        public ResultObject<Tbl_Admin_User> adminLoginAuth(string RequestType, Tbl_Admin_User data)
        {
            ResultObject<Tbl_Admin_User> resultObject = new ResultObject<Tbl_Admin_User>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Tbl_Admin_User>>(_IAdminDA.adminLoginAuth(RequestType, data));

            }
            catch (Exception ex)
            {
                  ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<Tbl_Stock_Symbol>> getStocks(string RequestType, string Id, string param, FilterData data)
        {
            ResultObject<List<Tbl_Stock_Symbol>> resultObject = new ResultObject<List<Tbl_Stock_Symbol>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<Tbl_Stock_Symbol>>>(_IAdminDA.getStocks(RequestType, Id, param, data));
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getStocks", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<Usersdetails>> getUser(string RequestType, string Id, string param, FilterData data)
        {
            ResultObject<List<Usersdetails>> resultObject = new ResultObject<List<Usersdetails>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<Usersdetails>>>(_IAdminDA.getUser(RequestType, Id, param, data));
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getUser", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> submitdetails(string RequestType, string Id, string param, string xml, string sp)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.submitdetails(RequestType, Id,param, xml,sp));
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "submitdetails", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> updateProfile(string RequestType, string param1, Tbl_Admin_User data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.updateProfile(RequestType, param1, data));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "updateProfile", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> updatePassword(string RequestType, string param1, string param2, string param3)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.updatePassword(RequestType, param1, param2, param3));

            }
            catch (Exception ex)
            {
                 ErrorLogs._InsertLogs(RequestType, "updatePassword", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<Tbl_Admin_Intrinsic_Values> adminvVal(string RequestType)
        {
            ResultObject<Tbl_Admin_Intrinsic_Values> resultObject = new ResultObject<Tbl_Admin_Intrinsic_Values>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Tbl_Admin_Intrinsic_Values>>(_IAdminDA.adminvVal(RequestType));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "adminvVal", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<CmsPageData>> cmsMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<CmsPageData>> resultObject = new ResultObject<List<CmsPageData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<CmsPageData>>>(_IAdminDA.cmsMaster(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "cmsMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editCmsMaster(string RequestType, string param, CmsPageData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editCmsMaster(RequestType, param, data));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "editCmsMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<ConfigurationData>> viewConfiguration(string RequestType, string param1, string param2)
        {
            ResultObject<List<ConfigurationData>> resultObject = new ResultObject<List<ConfigurationData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<ConfigurationData>>>(_IAdminDA.viewConfiguration(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "tetimonial", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editConfiguration(string RequestType, string param, ConfigurationData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editConfiguration(RequestType, param, data));
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "editConfiguration", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<FaqData>> viewfaq(string RequestType, string param1, string param2)
        {
            ResultObject<List<FaqData>> resultObject = new ResultObject<List<FaqData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<FaqData>>>(_IAdminDA.viewfaq(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "tetimonial", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editfaq(string RequestType, string param, FaqData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editfaq(RequestType, param, data));
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "editConfiguration", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<tbl_PageSEO>> viewSeo(string RequestType, string param1, string param2)
        {
            ResultObject<List<tbl_PageSEO>> resultObject = new ResultObject<List<tbl_PageSEO>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<tbl_PageSEO>>>(_IAdminDA.viewSeo(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewSeo", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<StockData>> getStocksDetals(string RequestType, string Id, string param, FilterData data)
        {
            ResultObject<List<StockData>> resultObject = new ResultObject<List<StockData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<StockData>>>(_IAdminDA.getStocksDetals(RequestType, Id, param, data));
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getStocks", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editStockProfile(string RequestType, string param, StockData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editStockProfile(RequestType, param, data));
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "editStockProfile", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }


        public ResultObject<List<Tbl_Seo_Stock>> viewSeoStock(string RequestType, string param1, string param2)
        {
            ResultObject<List<Tbl_Seo_Stock>> resultObject = new ResultObject<List<Tbl_Seo_Stock>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<Tbl_Seo_Stock>>>(_IAdminDA.viewSeoStock(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewSeo", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
    }
}

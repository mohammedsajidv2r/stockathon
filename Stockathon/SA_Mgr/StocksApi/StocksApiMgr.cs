﻿using AutoMapper;
using SA_BaseObjet;
using SA_BaseObjet.StocksApi;
using SA_DA_Interface.StocksApi;
using SA_Mgr_Interface.StocksApi;
using SA_Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SA_Mgr.StocksApi
{
    public class StocksApiMgr : IStocksApiMgr
    {
        IStocksApiDA _IStocksApiDA;
        IMapper _mapper;

        public StocksApiMgr(IMapper IMapper, IStocksApiDA IStocksApiDA)
        {
            _IStocksApiDA = IStocksApiDA;
            _mapper = IMapper;
        }

      public ResultObject<List<Tbl_Stock_Symbol>> getStocks(string RequestType, string Id, string param)
      {
          ResultObject<List<Tbl_Stock_Symbol>> resultObject = new ResultObject<List<Tbl_Stock_Symbol>>();
          try
          {
              resultObject = _mapper.Map<ResultObject<List<Tbl_Stock_Symbol>>>(_IStocksApiDA.getStocks(RequestType, Id, param));
          }
          catch (Exception ex)
          {
                ErrorLogs._InsertLogs(RequestType, "getStocks", "StocksApiMgr", 0, ex.Message.ToString());
            }
          return resultObject;
      }

        public ResultObject<string> submitdetails(string RequestType, string Id, string xml)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IStocksApiDA.submitdetails(RequestType, Id, xml));
            }
            catch (Exception ex)
            {
                 ErrorLogs._InsertLogs(RequestType, "submitdetails", "StocksApiMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<Usersdetails> signup(string RequestType, Usersdetails data)
        {
            ResultObject<Usersdetails> resultObject = new ResultObject<Usersdetails>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Usersdetails>>(_IStocksApiDA.signup(RequestType, data));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "signup", "StocksApiMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<Financial> getFinancial(string RequestType, string Id, string param, string xml)
        {
            ResultObject<Financial> resultObject = new ResultObject<Financial>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Financial>>(_IStocksApiDA.getFinancial(RequestType, Id, param, xml));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getFinancial", "StocksApiMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<dynamic>> getData(string RequestType, string Id, string param)
        {
            ResultObject<List<dynamic>> resultObject = new ResultObject<List<dynamic>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<dynamic>>>(_IStocksApiDA.getData(RequestType, Id, param));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getData", "StocksApiMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<DynamicList> getPeer(string RequestType, string Id, string param)
        {
            ResultObject<DynamicList> resultObject = new ResultObject<DynamicList>();
            try
            {
                resultObject = _mapper.Map<ResultObject<DynamicList>>(_IStocksApiDA.getPeer(RequestType, Id, param));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getPeer", "StocksApiMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> updatedetails(string RequestType, string Id, string xml, string param)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IStocksApiDA.updatedetails(RequestType, Id, xml, param));
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "updatedetails", "StocksApiMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> savedetails(string RequestType, string Id, string xml, string param, string sp)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IStocksApiDA.savedetails(RequestType, Id, xml, param,sp));
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "updatedetails", "StocksApiMgr", 0, ex.Message.ToString());
            }
            return resultObject;
           
        }

        public ResultObject<DynamicList> getDynamic5(string RequestType, string Id, string param, string sp)
        {
            ResultObject<DynamicList> resultObject = new ResultObject<DynamicList>();
            try
            {
                resultObject = _mapper.Map<ResultObject<DynamicList>>(_IStocksApiDA.getDynamic5(RequestType, Id, param, sp));

            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getDynamic5", "StocksApiMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SA_BaseObjet.StocksApi
{
    public class Tbl_Stock_Symbol
    {
        public long stock_symbol_id { get; set; }

        public string symbol { get; set; }

        public string description { get; set; }

        public string currency { get; set; }
        public string displaySymbol { get; set; }

        public string type { get; set; }
        public decimal close { get; set; }
        public decimal changeper { get; set; }

    }

}

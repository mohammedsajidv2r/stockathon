﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SA_BaseObjet.StocksApi
{
   public class Quote
    {
        public long quote_id { get; set; }

        public decimal? c { get; set; }

        public decimal? h { get; set; }

        public decimal? l { get; set; }

        public decimal? o { get; set; }

        public DateTime? t { get; set; }

        public string symbol { get; set; }

        public long? stock_symbol_id { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }
    }
}

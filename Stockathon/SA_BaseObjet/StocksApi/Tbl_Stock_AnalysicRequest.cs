﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SA_BaseObjet.StocksApi
{
    public class Tbl_Stock_AnalysicRequest
    {
        public int req_id { get; set; }

        public string first_name { get; set; }
        public string last_name { get; set; }

        public string email { get; set; }

        public string phone { get; set; }

        public string subject { get; set; }

        public string message { get; set; }

    }
}

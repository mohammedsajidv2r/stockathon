﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SA_BaseObjet.StocksApi
{
   public class Usersdetails
    {
        public long user_id { get; set; }

        public string mobile { get; set; }

        public string first_name { get; set; }

        public string last_name { get; set; }

        public string email { get; set; }

        public short? email_is_verified { get; set; }

        public short? status { get; set; }

        public DateTime? last_login { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }
        public string email_link { get; set; }
        public int social_flag { get; set; }
        public string social_token { get; set; }
        public string userCount { get; set; }
        public int login_flag { get; set; }
        public string prime_status { get; set; }

        public string membership_flag { get; set; }
        public string plan_type { get; set; }
    }
}

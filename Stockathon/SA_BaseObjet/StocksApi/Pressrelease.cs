﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SA_BaseObjet.StocksApi
{
    public class Pressrelease
    {
        public List<majorDevelopment> majorDevelopment { get; set; }
    }

    public class majorDevelopment
    {
        public string symbol { get; set; }
        public string datetime { get; set; }
        public string headline { get; set; }
        public string description { get; set; }
    }
}

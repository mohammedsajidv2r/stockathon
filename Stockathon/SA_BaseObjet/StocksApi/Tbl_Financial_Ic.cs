﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SA_BaseObjet.StocksApi
{
    public class Financial
    {
        public  List<pnl> pnl { get;set;}
        public List<balanceShhet> balanceShhet { get; set; }

        public List<cashFlow> cashFlow { get; set; }
        public description description { get; set; }
    }
    public class pnl
    {
        public long financial_Ic_id { get; set; }

        public string freq { get; set; }

        public string symbol { get; set; }

        public long? stock_symbol_id { get; set; }

        public int? year { get; set; }

        public string period { get; set; }

        public int? period_year { get; set; }

        public int? period_month { get; set; }

        public int? period_day { get; set; }

        public DateTime? created_at { get; set; }

        public decimal? costOfGoodsSold { get; set; }

        public decimal? dilutedAverageSharesOutstanding { get; set; }

        public decimal? dilutedEPS { get; set; }

        public decimal? ebit { get; set; }

        public decimal? gainLossSaleOfAssets { get; set; }

        public decimal? grossIncome { get; set; }

        public decimal? interestIncomeExpense { get; set; }

        public decimal? netIncome { get; set; }

        public decimal? netIncomeAfterTaxes { get; set; }

        public decimal? pretaxIncome { get; set; }

        public decimal? provisionforIncomeTaxes { get; set; }

        public decimal? researchDevelopment { get; set; }

        public decimal? revenue { get; set; }

        public decimal? sgaExpense { get; set; }

        public decimal? totalOperatingExpense { get; set; }

        public decimal? totalOtherIncomeExpenseNet { get; set; }
        public decimal? grossProfiltPer { get; set; }
        public decimal? ebitPer { get; set; }
    }

    public class balanceShhet
    {
        public long financial_Bs_id { get; set; }

        public string freq { get; set; }

        public string symbol { get; set; }

        public long? stock_symbol_id { get; set; }

        public string year { get; set; }

        public string period { get; set; }

        public string period_year { get; set; }

        public string period_month { get; set; }

        public string period_day { get; set; }

        public DateTime? created_at { get; set; }

        public decimal? accountsPayable { get; set; }

        public decimal? accountsReceivables { get; set; }

        public decimal? accruedLiability { get; set; }

        public decimal? accumulatedDepreciation { get; set; }

        public decimal? cash { get; set; }

        public decimal? cashEquivalents { get; set; }

        public decimal? cashShortTermInvestments { get; set; }

        public decimal? commonStock { get; set; }

        public decimal? currentAssets { get; set; }

        public decimal? currentLiabilities { get; set; }

        public decimal? currentPortionLongTermDebt { get; set; }

        public decimal? deferredIncomeTax { get; set; }

        public decimal? goodwill { get; set; }

        public decimal? intangiblesAssets { get; set; }

        public decimal? inventory { get; set; }

        public decimal? liabilitiesShareholdersEquity { get; set; }

        public decimal? longTermDebt { get; set; }

        public decimal? longTermInvestments { get; set; }

        public decimal? nonRedeemablePreferredStock { get; set; }

        public decimal? otherCurrentAssets { get; set; }

        public decimal? otherCurrentliabilities { get; set; }

        public decimal? otherEquity { get; set; }

        public decimal? otherLiabilities { get; set; }

        public decimal? otherLongTermAssets { get; set; }

        public decimal? otherReceivables { get; set; }

        public decimal? preferredSharesOutstanding { get; set; }

        public decimal? propertyPlantEquipment { get; set; }

        public decimal? retainedEarnings { get; set; }

        public decimal? sharesOutstanding { get; set; }

        public decimal? shortTermDebt { get; set; }

        public decimal? shortTermInvestments { get; set; }

        public decimal? tangibleBookValueperShare { get; set; }

        public decimal? totalAssets { get; set; }

        public decimal? totalDebt { get; set; }

        public decimal? totalEquity { get; set; }

        public decimal? totalLiabilities { get; set; }

        public decimal? totalLongTermDebt { get; set; }

        public decimal? totalReceivables { get; set; }

        public decimal? unrealizedProfitLossSecurity { get; set; }

        public decimal? treasuryStock { get; set; }

        public decimal? noteReceivableLongTerm { get; set; }
        public decimal? miscellaneous { get; set; }
    }

    public class cashFlow
    {
        public long financial_cf_id { get; set; }

        public string freq { get; set; }

        public string symbol { get; set; }

        public long? stock_symbol_id { get; set; }

        public int? year { get; set; }

        public string period { get; set; }

        public int? period_year { get; set; }

        public int? period_month { get; set; }

        public int? period_day { get; set; }

        public DateTime? created_at { get; set; }

        public decimal? amortization { get; set; }

        public decimal? capex { get; set; }

        public decimal? cashDividendsPaid { get; set; }

        public decimal? cashInterestPaid { get; set; }

        public decimal? cashTaxesPaid { get; set; }

        public decimal? changeinCash { get; set; }

        public decimal? changesinWorkingCapital { get; set; }

        public decimal? deferredTaxesInvestmentTaxCredit { get; set; }

        public decimal? depreciationAmortization { get; set; }

        public decimal? foreignExchangeEffects { get; set; }

        public decimal? issuanceReductionCapitalStock { get; set; }

        public decimal? issuanceReductionDebtNet { get; set; }

        public decimal? netCashFinancingActivities { get; set; }

        public decimal? netIncomeStartingLine { get; set; }

        public decimal? netInvestingCashFlow { get; set; }

        public decimal? netOperatingCashFlow { get; set; }

        public decimal? otherFundsFinancingItems { get; set; }

        public decimal? otherFundsNonCashItems { get; set; }

        public decimal? otherInvestingCashFlowItemsTotal { get; set; }
    }

    public class description
    {
        public string stock_name { get; set; }
        public string symbol { get; set; }
        public string address { get; set; }
        public int employeeTotal { get; set; }
        public string exchange { get; set; }
        public string finnhubIndustry { get; set; }
        public string gsector { get; set; }
        public DateTime ipo { get; set; }
        public string isin { get; set; }
        public string company_description { get; set; }
        public decimal marketCapitalization { get; set; }
        public string weburl { get; set; }
        public string logo { get; set; }
        public string ticker { get; set; }
        public string gsubind { get; set; }
        public decimal _52WeekHigh { get; set; }
        public decimal _52WeekLow { get; set; }
        public decimal beta { get; set; }
        public decimal peNormalizedAnnual { get; set; }
        public decimal pbAnnual { get; set; }
        public decimal roiAnnual { get; set; }
        public decimal currentDividendYieldTTM { get; set; }
        public decimal currentRatioAnnual { get; set; }
        public decimal quickRatioAnnual { get; set; }
        public decimal shareOutstanding { get; set; }
        public decimal close { get; set; }
        public decimal change { get; set; }
        public decimal changeper { get; set; }

        public string page_name { get; set; }
        public string page_title { get; set; }
        public string page_keyword { get; set; }
        public string page_description { get; set; }
    }
}

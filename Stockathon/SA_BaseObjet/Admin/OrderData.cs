﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SA_BaseObjet.Admin
{
    public class OrderData
    {
        public string order_id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string order_date_time { get; set; }
        public string txn_payment_status { get; set; }
        public string PlanStatus { get; set; }
        public string order_total_with_gst { get; set; }
        public string id { get; set; }
        public string orderCount { get; set; }
    }
}

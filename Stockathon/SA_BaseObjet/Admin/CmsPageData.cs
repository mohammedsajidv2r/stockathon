﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SA_BaseObjet.Admin
{
    public class CmsPageData
    {
        public int cms_id { get; set; }

        [Required(ErrorMessage = "Please Enter Page Title")]
        [MaxLength(100, ErrorMessage = "Title should be upto 100 characters.")]
        public string page_title { get; set; }
        
        public string page_content { get; set; }

        [Required(ErrorMessage = "Please Enter Page Meta Title")]
        [MaxLength(255, ErrorMessage = "Meta Title should be upto 255 characters.")]
        public string page_metatitle { get; set; }

        [MaxLength(500, ErrorMessage = "Meta Description should be upto 500 characters.")]
        public string page_metadescription { get; set; }


        [MaxLength(255, ErrorMessage = "Meta Keyword should be upto 255 characters.")]
        public string page_metakeywords { get; set; }
        public DateTime? created_at { get; set; }
    }
}

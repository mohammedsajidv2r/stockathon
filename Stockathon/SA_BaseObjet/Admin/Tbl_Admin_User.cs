﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SA_BaseObjet.Admin
{
    public class Tbl_Admin_User
    {
        public long admin_id { get; set; }

        [Required(ErrorMessage = "Please Enter First Name")]
        public string first_name { get; set; }

        public string Last_name { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$", ErrorMessage = "Please enter valid email address")]

        public string email { get; set; }

        [Required(ErrorMessage = "Please Enter Password")]
        [MinLength(6, ErrorMessage = "Password should contain atleast 6 characters.")]
        public string password { get; set; }

        public DateTime? last_login { get; set; }

        public string profile_picture { get; set; }
    }
}

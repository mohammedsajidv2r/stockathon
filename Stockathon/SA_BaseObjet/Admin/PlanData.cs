﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SA_BaseObjet.Admin
{
    public class PlanData
    {
        public long plan_features_id { get; set; }

        [Required(ErrorMessage = "Please Enter Plan Name")]
        public string plan_name { get; set; }

        public string plan_feature { get; set; }

        [Required(ErrorMessage = "Please Enter Duration")]
        public int plan_duration { get; set; }

        [Required(ErrorMessage = "Please Enter Price")]
        public decimal plan_pricing { get; set; }

        [Required(ErrorMessage = "Please Enter discount price")]
        public decimal plan_old_price { get; set; }

        public string plan_short_message { get; set; }

        public bool IsActive { get; set; }
        public int Plan_Flag { get; set; }
        public string feature_short_code { get; set; }
        public int Plan_type { get; set; }
    }
}

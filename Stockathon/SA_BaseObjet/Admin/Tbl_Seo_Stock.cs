﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SA_BaseObjet.Admin
{
    public class Tbl_Seo_Stock
    {
        public int PageId { get; set; }

        [Required(ErrorMessage = "Please Enter Page Keyword")]
        public string Page_Keyword { get; set; }

        [Required(ErrorMessage = "Please Enter Page Description")]
        public string Page_Description { get; set; }

        public string Page_Name { get; set; }

        public string SourcePage_Name { get; set; }

        [Required(ErrorMessage = "Please Enter Page Title")]
        public string Page_Title { get; set; }

        [Required(ErrorMessage = "Please Select Symbol")]
        public string symbol { get; set; }

        public string Page_Canonical { get; set; }
        public string page_content { get; set; }
        public bool Content_Type { get; set; }

        public DateTime? created_at { get; set; }
    }
}

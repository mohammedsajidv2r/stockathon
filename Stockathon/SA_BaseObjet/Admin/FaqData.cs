﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SA_BaseObjet.Admin
{
    public class FaqData
    {
        public int faq_id { get; set; }

        [Required(ErrorMessage = "Please Enter Question")]
        [MaxLength(200, ErrorMessage = "Question should be upto 200 characters.")]
        public string faq_question { get; set; }

        public string faq_answer { get; set; }

        public bool active { get; set; }
        public int RowNo { get; set; }
    }
}

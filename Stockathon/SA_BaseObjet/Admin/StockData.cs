﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SA_BaseObjet.Admin
{
    public class StockData
    {
        public long stock_symbol_id { get; set; }

        public string symbol { get; set; }

        [Required(ErrorMessage = "Please Enter Stock Description")]
        //[MaxLength(100, ErrorMessage = "Title should be upto 100 characters.")]
        public string description { get; set; }

        public string currency { get; set; }
        public string displaySymbol { get; set; }

        public string type { get; set; }
        public decimal close { get; set; }
        public decimal changeper { get; set; }
    }
}

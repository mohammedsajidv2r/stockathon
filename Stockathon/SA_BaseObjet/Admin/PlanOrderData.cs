﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SA_BaseObjet.Admin
{
    public class PlanOrderData
    {

        [Required(ErrorMessage = "Please Select Plan")]
        public string plan_name { get; set; }
        public string plan_duration { get; set; }
        public string order_id { get; set; }
        public string txn_payment_id { get; set; }
        public string txn_payment_status { get; set; }
        public string order_date_time { get; set; }
        public string start_date { get; set; }
        public DateTime? start_dateDt { get; set; }
        public string end_date { get; set; }
        public DateTime? end_dateDT { get; set; }
        public DateTime? end_date1 { get; set; }
        public string PlanFlag { get; set; }
        public string email { get; set; }
        public string membership_id { get; set; }
        public string plan_id_list { get; set; }
        public string user_id { get; set; }

        public string order_plans_json { get; set; }

        public string plan_features_id { get; set; }

        public string selectPlan { get; set; }

    }
}

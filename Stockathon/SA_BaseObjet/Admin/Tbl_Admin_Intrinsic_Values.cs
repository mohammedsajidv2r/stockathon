﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SA_BaseObjet.Admin
{
    public class Tbl_Admin_Intrinsic_Values
    {
        public string id { get; set; }
        [Required(ErrorMessage = "Please Enter Rf Value")]
        public string Rf { get; set; }

        [Required(ErrorMessage = "Please Enter Y Value")]
        public string  Y { get; set; }

        [Required(ErrorMessage = "Please Enter Rm Value")]
        public string  Rm { get; set; }

        public DateTime? updated_at { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SA_BaseObjet.Admin
{
    public class ConfigurationData
    {
        public int config_id { get; set; }

        [Required(ErrorMessage = "Please Enter Twitter Link")]
        [Url(ErrorMessage = "Please Enter a valid url")]
        [MaxLength(100, ErrorMessage = "Twitter Link should be upto 100 characters.")]
        public string config_twitter { get; set; }

        [Required(ErrorMessage = "Please Enter Facebook Link")]
        [Url(ErrorMessage = "Please Enter a valid url")]
        [MaxLength(100, ErrorMessage = "Facebook Link should be upto 100 characters.")]
        public string config_facebook { get; set; }

        [Required(ErrorMessage = "Please Enter Instagram Link")]
        [Url(ErrorMessage = "Please Enter a valid url")]
        [MaxLength(100, ErrorMessage = "Instagram Link should be upto 100 characters.")]
        public string config_instagram { get; set; }

        [Required(ErrorMessage = "Please Enter Linkedin Link")]
        [Url(ErrorMessage = "Please Enter a valid url")]
        [MaxLength(100, ErrorMessage = "Linkedin Link should be upto 100 characters.")]
        public string config_linkedin { get; set; }

        [Required(ErrorMessage = "Please Enter Contact number")]
        //[RegularExpression(@"^[0-9*#+]+$", ErrorMessage = "Contact number should contain digits.")]
        [RegularExpression(@"^[0-9-+()]*$", ErrorMessage = "Contact number should contain digits.")]
        [MaxLength(15, ErrorMessage = "Contact number should be upto 15 digits.")]
        public string config_contact { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$", ErrorMessage = "Please enter valid email address")]
        [MaxLength(50, ErrorMessage = "Email should be upto 50 characters.")]
        public string config_email { get; set; }


        //[Required(ErrorMessage = "Please Enter Website")]
        //[Url(ErrorMessage = "Please enter a valid url")]
        //[MaxLength(100, ErrorMessage = "Website should be upto 100 characters.")]
        public string config_website { get; set; }

        [Required(ErrorMessage = "Please Enter Location")]
        [MaxLength(200, ErrorMessage = "Location should be upto 200 characters.")]
        public string config_location { get; set; }

    }
}

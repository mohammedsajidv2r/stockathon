﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SA_BaseObjet.Admin
{
    public class FilterData
    {
        public string start { get; set; }
        public string length { get; set; }
        public string sort { get; set; }
        public string userType { get; set; }
    }
}

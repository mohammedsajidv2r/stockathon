﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SA_BaseObjet.PostModel
{
    public class PostDataModel
    {
        public class stockPost
        {
            public string symbol { get; set; }
            public string dataType { get; set; }
            public string type { get; set; }
            public string tableType { get; set; }

            public string fromDate { get; set; }
            public string toDate { get; set; }
        }

        public class peerPost
        {
            public string peerColumns { get; set; }
            public string screenerColumns { get; set; }
        }

        public class userOrderPostData
        {
            public decimal order_total_without_gst { get; set; }
            public decimal order_total_with_gst { get; set; }
            public string txn_payment_id { get; set; }
            public string txn_payment_status { get; set; }
            public string order_plans_json { get; set; }
            public string txn_json { get; set; }
            public int plan_id { get; set; }
        }
    }
}

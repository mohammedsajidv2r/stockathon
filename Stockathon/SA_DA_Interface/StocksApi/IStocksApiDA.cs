﻿using SA_BaseObjet;
using SA_BaseObjet.StocksApi;
using System;
using System.Collections.Generic;
using System.Text;

namespace SA_DA_Interface.StocksApi
{
    public interface IStocksApiDA
    {
        ResultObject<List<Tbl_Stock_Symbol>> getStocks(string RequestType, string Id, string param);
        ResultObject<string> submitdetails(string RequestType, string Id, string xml);

        ResultObject<Usersdetails> signup(string RequestType, Usersdetails data);

        ResultObject<Financial> getFinancial(string RequestType, string Id, string param, string xml);
        ResultObject<List<dynamic>> getData(string RequestType, string Id, string param);

        ResultObject<DynamicList> getPeer(string RequestType, string Id, string param);
        ResultObject<string> updatedetails(string RequestType, string Id, string xml, string param);
        ResultObject<string> savedetails(string RequestType, string Id, string xml, string param, string sp);
        ResultObject<DynamicList> getDynamic5(string RequestType, string Id, string param, string sp);
    }
}

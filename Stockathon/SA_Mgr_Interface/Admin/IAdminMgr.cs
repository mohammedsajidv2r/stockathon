﻿using SA_BaseObjet;
using SA_BaseObjet.Admin;
using SA_BaseObjet.StocksApi;
using System;
using System.Collections.Generic;
using System.Text;

namespace SA_Mgr_Interface.Admin
{
    public interface IAdminMgr
    {
        ResultObject<Tbl_Admin_User> adminLoginAuth(string RequestType, Tbl_Admin_User data);
        ResultObject<List<Tbl_Stock_Symbol>> getStocks(string RequestType, string Id, string param, FilterData data);

        ResultObject<string> submitdetails(string RequestType, string Id, string param, string xml, string sp);

        ResultObject<List<Usersdetails>> getUser(string RequestType, string Id, string param, FilterData data);
        ResultObject<string> updateProfile(string RequestType, string param1, Tbl_Admin_User data);

        ResultObject<string> updatePassword(string RequestType, string param1, string param2, string param3);

        ResultObject<Tbl_Admin_Intrinsic_Values> adminvVal(string RequestType);

        ResultObject<List<CmsPageData>> cmsMaster(string RequestType, string param1, string param2);

        ResultObject<string> editCmsMaster(string RequestType, string param, CmsPageData data);

        ResultObject<List<ConfigurationData>> viewConfiguration(string RequestType, string param1, string param2);

        ResultObject<string> editConfiguration(string RequestType, string param, ConfigurationData data);

        ResultObject<List<FaqData>> viewfaq(string RequestType, string param1, string param2);

        ResultObject<string> editfaq(string RequestType, string param, FaqData data);

        ResultObject<List<tbl_PageSEO>> viewSeo(string RequestType, string param1, string param2);


        ResultObject<List<StockData>> getStocksDetals(string RequestType, string Id, string param, FilterData data);


        ResultObject<string> editStockProfile(string RequestType, string param, StockData data);


        ResultObject<List<Tbl_Seo_Stock>> viewSeoStock(string RequestType, string param1, string param2);

    }
}

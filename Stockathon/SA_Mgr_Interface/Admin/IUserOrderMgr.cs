﻿using SA_BaseObjet;
using SA_BaseObjet.Admin;
using System;
using System.Collections.Generic;
using System.Text;

namespace SA_Mgr_Interface.Admin
{
    public interface IUserOrderMgr
    {
        ResultObject<List<PlanOrderData>> viewPlanDetail(string RequestType, string param);
        ResultObject<string> userPlanUpdate(string RequestType, List<PlanOrderData> data);
        ResultObject<List<PlanData>> viewPlan(string RequestType, string param1, string param2);
        ResultObject<string> updatedetails(string RequestType, string Id, string xml, string param, string sp);

        ResultObject<List<OrderData>> viewOrderDetailFilter(string RequestType, FilterData data);
    }
}

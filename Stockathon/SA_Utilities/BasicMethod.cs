﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SA_Utilities
{
    public class BasicMethod
    {
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            long unixTimeStampInTicks = (long)(unixTimeStamp * TimeSpan.TicksPerSecond);
            return new DateTime(unixStart.Ticks + unixTimeStampInTicks, System.DateTimeKind.Utc);
        }

        public static string RemoveDiacritics(string value)
        {
            var bytes = Encoding.Default.GetBytes(value);
            var text = Encoding.UTF8.GetString(bytes);
            return text;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace SA_Utilities
{
    public static class ErrorLogs
    {
        public static bool _InsertLogs(string RequestType, string Name, string Type, int? ErrorLine, string Error_message)
        {
            SqlConnection con = new SqlConnection(@"Data Source=niftytrader-sql-server.cc5xca7lpn9p.ap-south-1.rds.amazonaws.com;Initial Catalog=stockathon;User ID=niftysqlrdsuser;Password=niftysqlrds2684!!!");
            try
            {

                SqlCommand cmd = new SqlCommand("USP_Logs", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Request_type", RequestType);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@Type", Type);
                cmd.Parameters.AddWithValue("@Error_Line", ErrorLine);
                cmd.Parameters.AddWithValue("@ErrorMsg", Error_message);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
            return true;
        }
    }
}

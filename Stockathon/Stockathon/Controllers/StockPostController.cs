﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Runtime.Versioning;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SA_BaseObjet;
using SA_BaseObjet.StocksApi;
using SA_Mgr_Interface.StocksApi;
using SA_Utilities;
using SA_Utilities.Enum;
using static SA_BaseObjet.PostModel.PostDataModel;

namespace Stockathon.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class StockPostController : ControllerBase
    {
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IStocksApiMgr _IStocksApiMgr;

        public StockPostController(IStocksApiMgr IStocksApiMgr, IConfiguration iConfig, IHttpContextAccessor httpContextAccessor)
        {
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IStocksApiMgr = IStocksApiMgr;
        }


        [HttpPost]
        public dynamic getStockList(stockPost data)
        {
            ResultObject<List<Tbl_Stock_Symbol>> res = new ResultObject<List<Tbl_Stock_Symbol>>();
            try
            {
                if (data.symbol == "")
                {
                    res = _IStocksApiMgr.getStocks("selectsymbol", "", "");
                }
                else
                {
                    res = _IStocksApiMgr.getStocks("getstockssearch", data.symbol, "");
                }
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.Select(
                            x => new
                            {
                                x.symbol,
                                label = x.description,
                                x.displaySymbol,
                                x.close,
                                x.changeper
                            }
                            )
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getStockList", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }

        [HttpPost]
        public dynamic getPopularStockList(stockPost data)
        {
            ResultObject<List<Tbl_Stock_Symbol>> res = new ResultObject<List<Tbl_Stock_Symbol>>();
            try
            {
                if (data.type == "popular")
                {
                    res = _IStocksApiMgr.getStocks("selectpopularsymbol", "", "");
                }
                else
                {
                    res = _IStocksApiMgr.getStocks("viewallsymbol", "", "");
                }
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.Select(
                            x => new
                            {
                                x.symbol,
                                label = x.description,
                                x.displaySymbol
                            }
                            )
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getPopularStockList", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }


        [HttpPost]
        public dynamic submitStockReq(Tbl_Stock_AnalysicRequest data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                string subject = "Stock Enquiry";
                data.subject = subject;

                var XMLData = XmlConversion.SerializeToXElement(data);

                string body = "Hi Stockathon,"
                  + "<br /> You have receive an enquiry from " + data.first_name + " " + data.last_name
                  + "<br /> Please find below details."
                  + "<br /> Email Address: " + data.email
                  + "<br /> Message: " + data.message
                  + "<br /> Thank You";

                res = _IStocksApiMgr.submitdetails("submitStockReq", "", XMLData.ToString());
                if (res.Result == ResultType.Success)
                {
                    Task.Factory.StartNew(() => SentEmail.emailSent(body, subject, SentEmail.fromemail, SentEmail.fromemail));
                    return new
                    {
                        result = 1,
                        message = "Success"
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("submitStockReq", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Failure"
                } as dynamic;
            }
        }


        [HttpPost]
        public dynamic signUp(Usersdetails data)
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                res = _IStocksApiMgr.signup("signupsocial", data);
                var tokenStr = GenerateJSONWebToken(res.ResultData);
                return new
                {
                    result = 1,
                    message = "Success",
                    data = new
                    {
                        token = tokenStr,
                        res.ResultData.mobile,
                        res.ResultData.first_name,
                        res.ResultData.last_name,
                        res.ResultData.email,
                        res.ResultData.email_is_verified,
                        res.ResultData.status,
                        res.ResultData.last_login,
                        res.ResultData.created_at,
                        res.ResultData.updated_at,
                        res.ResultData.user_id,
                        res.ResultData.membership_flag,
                        res.ResultData.plan_type
                    }
                } as dynamic;
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("signUp", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Failure"
                } as dynamic;
            }
        }

        [HttpPost]
        public dynamic getStockData(stockPost data)
        {
            ResultObject<Financial> res = new ResultObject<Financial>();
            try
            {
                if (data.symbol == "")
                {
                    return new
                    {
                        result = 0,
                        message = "Please Enter Symbol"
                    } as dynamic;
                }
                else
                {
                    if (data.type == "")
                    {
                        data.type = "annual";
                    }
                    else if (data.type == "quarterly")
                    {
                        data.type = "quarterly";
                    }
                    else
                    {
                        data.type = "annual";
                    }

                    res = _IStocksApiMgr.getFinancial("pnl", data.symbol, data.type, "");
                    if (res.Result == ResultType.Success)
                    {
                        List<Dictionary<string, object>> pnl = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> balanceSheet = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> cashflow = new List<Dictionary<string, object>>();

                        string[] blankArray = new string[] { };
                        if (data.tableType == "pnl")
                        {
                            #region pnl

                            //revenue
                            Dictionary<string, object> revenue = new Dictionary<string, object>();
                            revenue.Add("type", "Revenue");
                            foreach (var item in res.ResultData.pnl)
                            {
                                revenue.Add(item.period.ToString(), item.revenue);
                            }
                            revenue.Add("innerData", blankArray);
                            //costOfGoodsSold
                            Dictionary<string, object> costOfGoodsSold = new Dictionary<string, object>();
                            costOfGoodsSold.Add("type", "Cost of Goods Sold");
                            foreach (var item in res.ResultData.pnl)
                            {
                                costOfGoodsSold.Add(item.period.ToString(), item.costOfGoodsSold);
                            }
                            costOfGoodsSold.Add("innerData", blankArray);

                            //groosProfiit
                            Dictionary<string, object> groosProfiit = new Dictionary<string, object>();
                            groosProfiit.Add("type", "Gross Profit");
                            foreach (var item in res.ResultData.pnl)
                            {
                                groosProfiit.Add(item.period.ToString(), item.grossIncome);
                            }
                            groosProfiit.Add("innerData", blankArray);

                            //groosProfiitPer
                            Dictionary<string, object> groosProfiitPer = new Dictionary<string, object>();
                            groosProfiitPer.Add("type", "Gross Profit %");
                            foreach (var item in res.ResultData.pnl)
                            {
                                groosProfiitPer.Add(item.period.ToString(), item.grossProfiltPer);
                            }
                            groosProfiitPer.Add("innerData", blankArray);


                            //expance
                            Dictionary<string, object> totalOperatingExpense = new Dictionary<string, object>();
                            totalOperatingExpense.Add("type", "Total Operating Expense");
                            foreach (var item in res.ResultData.pnl)
                            {
                                totalOperatingExpense.Add(item.period.ToString(), item.totalOperatingExpense);
                            }
                            //inner expance
                            List<Dictionary<string, object>> expanceInner = new List<Dictionary<string, object>>();
                            Dictionary<string, object> researchDevelopment = new Dictionary<string, object>();
                            researchDevelopment.Add("type", "Research Development");
                            foreach (var item in res.ResultData.pnl)
                            {
                                researchDevelopment.Add(item.period, item.researchDevelopment);
                            }
                            Dictionary<string, object> sgaExpense = new Dictionary<string, object>();
                            sgaExpense.Add("type", "SGA Expense");
                            foreach (var item in res.ResultData.pnl)
                            {
                                sgaExpense.Add(item.period.ToString(), item.sgaExpense);
                            }
                            expanceInner.Add(researchDevelopment);
                            expanceInner.Add(sgaExpense);
                            totalOperatingExpense.Add("innerData", expanceInner);

                            //EBIT
                            Dictionary<string, object> EBIT = new Dictionary<string, object>();
                            EBIT.Add("type", "EBIT");
                            foreach (var item in res.ResultData.pnl)
                            {
                                EBIT.Add(item.period.ToString(), item.ebit);
                            }
                            EBIT.Add("innerData", blankArray);

                            //EBITper
                            Dictionary<string, object> EBITper = new Dictionary<string, object>();
                            EBITper.Add("type", "EBIT %");
                            foreach (var item in res.ResultData.pnl)
                            {
                                EBITper.Add(item.period.ToString(), item.ebitPer);
                            }
                            EBITper.Add("innerData", blankArray);

                            //interestIncomeExpense
                            Dictionary<string, object> interestIncomeExpense = new Dictionary<string, object>();
                            interestIncomeExpense.Add("type", "Interest Income");
                            foreach (var item in res.ResultData.pnl)
                            {
                                interestIncomeExpense.Add(item.period.ToString(), item.interestIncomeExpense);
                            }
                            interestIncomeExpense.Add("innerData", blankArray);

                            //totalOtherIncomeExpenseNet
                            Dictionary<string, object> totalOtherIncomeExpenseNet = new Dictionary<string, object>();
                            totalOtherIncomeExpenseNet.Add("type", "Other Income");
                            foreach (var item in res.ResultData.pnl)
                            {
                                totalOtherIncomeExpenseNet.Add(item.period.ToString(), item.totalOtherIncomeExpenseNet);
                            }
                            totalOtherIncomeExpenseNet.Add("innerData", blankArray);

                            //provisionforIncomeTaxes
                            Dictionary<string, object> provisionforIncomeTaxes = new Dictionary<string, object>();
                            provisionforIncomeTaxes.Add("type", "Taxes");
                            foreach (var item in res.ResultData.pnl)
                            {
                                provisionforIncomeTaxes.Add(item.period.ToString(), item.provisionforIncomeTaxes);
                            }
                            provisionforIncomeTaxes.Add("innerData", blankArray);

                            //net income with inner data
                            Dictionary<string, object> netIncome = new Dictionary<string, object>();
                            netIncome.Add("type", "Net Income");
                            foreach (var item in res.ResultData.pnl)
                            {
                                netIncome.Add(item.period.ToString(), item.netIncome);
                            }
                            netIncome.Add("innerData", blankArray);
                            //innner net inconmer

                            //List<Dictionary<string, object>> netIncomeInner = new List<Dictionary<string, object>>();
                            //netIncomeInner.Add(EBIT);
                            //netIncomeInner.Add(interestIncomeExpense);
                            //netIncomeInner.Add(provisionforIncomeTaxes);
                            //netIncomeInner.Add(totalOtherIncomeExpenseNet);
                            //netIncome.Add("innerData", netIncomeInner);

                            //EPS income with inner data
                            Dictionary<string, object> EPS = new Dictionary<string, object>();
                            EPS.Add("type", "EPS");
                            foreach (var item in res.ResultData.pnl)
                            {
                                EPS.Add(item.period.ToString(), item.dilutedEPS);
                            }
                            EPS.Add("innerData", blankArray);
                            //List<Dictionary<string, object>> EPSInner = new List<Dictionary<string, object>>();
                            //Dictionary<string, object> netIncome1 = new Dictionary<string, object>();
                            //netIncome1.Add("type", "Net Income");
                            //foreach (var item in res.ResultData.pnl)
                            //{
                            //    netIncome1.Add(item.period.ToString(), item.netIncome);
                            //}
                            //EPSInner.Add(netIncome1);
                            Dictionary<string, object> dilutedAverageSharesOutstanding = new Dictionary<string, object>();
                            dilutedAverageSharesOutstanding.Add("type", "Average Share Outstanding");
                            foreach (var item in res.ResultData.pnl)
                            {
                                dilutedAverageSharesOutstanding.Add(item.period.ToString(), item.dilutedAverageSharesOutstanding);
                            }
                            dilutedAverageSharesOutstanding.Add("innerData", blankArray);
                            //EPSInner.Add(dilutedAverageSharesOutstanding);
                            //EPS.Add("innerData", EPSInner);

                            pnl.Add(revenue);
                            pnl.Add(costOfGoodsSold);
                            pnl.Add(groosProfiit);
                            pnl.Add(groosProfiitPer);
                            pnl.Add(totalOperatingExpense);
                            pnl.Add(EBIT);
                            pnl.Add(EBITper);
                            pnl.Add(interestIncomeExpense);
                            pnl.Add(totalOtherIncomeExpenseNet);
                            pnl.Add(provisionforIncomeTaxes);
                            pnl.Add(netIncome);
                            pnl.Add(dilutedAverageSharesOutstanding);
                            pnl.Add(EPS);

                            #endregion
                        }
                        else if (data.tableType == "balsheet")
                        {
                            #region balanceSheet

                            //blankArray

                            //total assest
                            Dictionary<string, object> liabilitiesShareholdersEquity = new Dictionary<string, object>();
                            liabilitiesShareholdersEquity.Add("type", "Total Assets");
                            liabilitiesShareholdersEquity.Add("isTotal", 1);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                liabilitiesShareholdersEquity.Add(item.period.ToString(), item.liabilitiesShareholdersEquity);
                            }
                            liabilitiesShareholdersEquity.Add("innerData", blankArray);

                            //Current Assets with inner data
                            Dictionary<string, object> currentAssets = new Dictionary<string, object>();
                            currentAssets.Add("type", "Current Assets");
                            currentAssets.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                currentAssets.Add(item.period.ToString(), item.currentAssets);
                            }

                            List<Dictionary<string, object>> currentAssetsInner = new List<Dictionary<string, object>>();

                            Dictionary<string, object> cashShortTermInvestments = new Dictionary<string, object>();
                            cashShortTermInvestments.Add("type", "Cash Short Term Investments");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                cashShortTermInvestments.Add(item.period, item.cashShortTermInvestments);
                            }
                            currentAssetsInner.Add(cashShortTermInvestments);

                            Dictionary<string, object> cash = new Dictionary<string, object>();
                            cash.Add("type", "Cash");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                cash.Add(item.period, item.cash);
                            }
                            currentAssetsInner.Add(cash);

                            Dictionary<string, object> cashEquivalents = new Dictionary<string, object>();
                            cashEquivalents.Add("type", "Cash Equivalents");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                cashEquivalents.Add(item.period, item.cashEquivalents);
                            }
                            currentAssetsInner.Add(cashEquivalents);


                            Dictionary<string, object> shortTermInvestments = new Dictionary<string, object>();
                            shortTermInvestments.Add("type", "Short Term Investments");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                shortTermInvestments.Add(item.period, item.shortTermInvestments);
                            }
                            currentAssetsInner.Add(shortTermInvestments);

                            Dictionary<string, object> totalReceivables = new Dictionary<string, object>();
                            totalReceivables.Add("type", "A/C Receivables");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                totalReceivables.Add(item.period, item.totalReceivables);
                            }
                            currentAssetsInner.Add(totalReceivables);

                            Dictionary<string, object> inventory = new Dictionary<string, object>();
                            inventory.Add("type", "Inventory");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                inventory.Add(item.period, item.inventory);
                            }
                            currentAssetsInner.Add(inventory);

                            Dictionary<string, object> otherCurrentAssets = new Dictionary<string, object>();
                            otherCurrentAssets.Add("type", "Other Current Assets");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherCurrentAssets.Add(item.period, item.otherCurrentAssets);
                            }
                            currentAssetsInner.Add(otherCurrentAssets);

                            Dictionary<string, object> otherReceivables = new Dictionary<string, object>();
                            otherReceivables.Add("type", "Other Receivables");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherReceivables.Add(item.period, item.otherReceivables);
                            }
                            currentAssetsInner.Add(otherReceivables);

                            currentAssets.Add("innerData", currentAssetsInner);
                            //Current Assets with inner data end


                            //Intangibles with inner data
                            Dictionary<string, object> intangiblesAssets = new Dictionary<string, object>();
                            intangiblesAssets.Add("type", "Intangibles Assets");
                            intangiblesAssets.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                intangiblesAssets.Add(item.period, item.goodwill + item.intangiblesAssets);
                            }

                            List<Dictionary<string, object>> intangiblesAssetsInner = new List<Dictionary<string, object>>();
                            Dictionary<string, object> goodwill = new Dictionary<string, object>();
                            goodwill.Add("type", "Goodwill");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                goodwill.Add(item.period, item.goodwill);
                            }
                            intangiblesAssetsInner.Add(goodwill);

                            Dictionary<string, object> otherintangiblesAssets = new Dictionary<string, object>();
                            otherintangiblesAssets.Add("type", "Other Intangibles Assets");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherintangiblesAssets.Add(item.period, item.intangiblesAssets);
                            }
                            intangiblesAssetsInner.Add(otherintangiblesAssets);

                            intangiblesAssets.Add("innerData", intangiblesAssetsInner);
                            //Intangibles with inner data end

                            //LT with inner data
                            Dictionary<string, object> LTAssets = new Dictionary<string, object>();
                            LTAssets.Add("type", "LT Assets");
                            LTAssets.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                LTAssets.Add(item.period, item.noteReceivableLongTerm + item.longTermInvestments + item.otherLongTermAssets);
                            }

                            List<Dictionary<string, object>> LTAssetsInner = new List<Dictionary<string, object>>();

                            Dictionary<string, object> noteReceivableLongTerm = new Dictionary<string, object>();
                            noteReceivableLongTerm.Add("type", "Note Receivable Long Term");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                noteReceivableLongTerm.Add(item.period, item.noteReceivableLongTerm);
                            }
                            LTAssetsInner.Add(noteReceivableLongTerm);

                            Dictionary<string, object> longTermInvestments = new Dictionary<string, object>();
                            longTermInvestments.Add("type", "Long Term Investments");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                longTermInvestments.Add(item.period, item.longTermInvestments);
                            }
                            LTAssetsInner.Add(longTermInvestments);

                            Dictionary<string, object> otherLongTermAssets = new Dictionary<string, object>();
                            otherLongTermAssets.Add("type", "Other Long Term Assets");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherLongTermAssets.Add(item.period, item.otherLongTermAssets);
                            }
                            LTAssetsInner.Add(otherLongTermAssets);
                            LTAssets.Add("innerData", LTAssetsInner);

                            //LT with inner dataend

                            //propertyPlantEquipment
                            Dictionary<string, object> propertyPlantEquipment = new Dictionary<string, object>();
                            propertyPlantEquipment.Add("type", "Property Plant Equipment");
                            propertyPlantEquipment.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                propertyPlantEquipment.Add(item.period, item.propertyPlantEquipment);
                            }
                            propertyPlantEquipment.Add("innerData", blankArray);


                            //totalLiabilities
                            Dictionary<string, object> totalLiabilities = new Dictionary<string, object>();
                            totalLiabilities.Add("type", "Total Liabilities");
                            totalLiabilities.Add("isTotal", 1);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                totalLiabilities.Add(item.period, item.totalLiabilities);
                            }
                            totalLiabilities.Add("innerData", blankArray);

                            //equity with inner data
                            Dictionary<string, object> totalEquity = new Dictionary<string, object>();
                            totalEquity.Add("type", "Total Equity");
                            totalEquity.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                totalEquity.Add(item.period, item.totalEquity);
                            }

                            List<Dictionary<string, object>> totalEquityInner = new List<Dictionary<string, object>>();

                            Dictionary<string, object> commonStock = new Dictionary<string, object>();
                            commonStock.Add("type", "Common Stock");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                commonStock.Add(item.period, item.commonStock);
                            }
                            totalEquityInner.Add(commonStock);

                            Dictionary<string, object> otherEquity = new Dictionary<string, object>();
                            otherEquity.Add("type", "Other Equity");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherEquity.Add(item.period, item.otherEquity);
                            }
                            totalEquityInner.Add(otherEquity);

                            Dictionary<string, object> retainedEarnings = new Dictionary<string, object>();
                            retainedEarnings.Add("type", "Retained Earning");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                retainedEarnings.Add(item.period, item.retainedEarnings);
                            }
                            totalEquityInner.Add(retainedEarnings);

                            Dictionary<string, object> unrealizedProfitLossSecurity = new Dictionary<string, object>();
                            unrealizedProfitLossSecurity.Add("type", "Unrealized Profit Loss Security");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                unrealizedProfitLossSecurity.Add(item.period, item.unrealizedProfitLossSecurity);
                            }
                            totalEquityInner.Add(unrealizedProfitLossSecurity);

                            Dictionary<string, object> miscellaneous = new Dictionary<string, object>();
                            miscellaneous.Add("type", "Miscellaneous");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                miscellaneous.Add(item.period, item.miscellaneous);
                            }
                            totalEquityInner.Add(miscellaneous);
                            totalEquity.Add("innerData", totalEquityInner);
                            //totalEquity  end


                            //equity with inner data
                            Dictionary<string, object> currentLiabilities = new Dictionary<string, object>();
                            currentLiabilities.Add("type", "Current Liabilities");
                            currentLiabilities.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                currentLiabilities.Add(item.period, item.currentLiabilities);
                            }

                            List<Dictionary<string, object>> currentLiabilitiesInner = new List<Dictionary<string, object>>();

                            Dictionary<string, object> accountsPayable = new Dictionary<string, object>();
                            accountsPayable.Add("type", "Accounts Payable");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                accountsPayable.Add(item.period, item.accountsPayable);
                            }
                            currentLiabilitiesInner.Add(accountsPayable);


                            Dictionary<string, object> accruedLiability = new Dictionary<string, object>();
                            accruedLiability.Add("type", "Accrued Liability");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                accruedLiability.Add(item.period, item.accruedLiability);
                            }
                            currentLiabilitiesInner.Add(accruedLiability);

                            Dictionary<string, object> currentPortionLongTermDebt = new Dictionary<string, object>();
                            currentPortionLongTermDebt.Add("type", "Current Portion Long Term Debt");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                currentPortionLongTermDebt.Add(item.period, item.currentPortionLongTermDebt);
                            }
                            currentLiabilitiesInner.Add(currentPortionLongTermDebt);

                            Dictionary<string, object> shortTermDebt = new Dictionary<string, object>();
                            shortTermDebt.Add("type", "Short Term Debt");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                shortTermDebt.Add(item.period, item.shortTermDebt);
                            }
                            currentLiabilitiesInner.Add(shortTermDebt);

                            Dictionary<string, object> otherCurrentliabilities = new Dictionary<string, object>();
                            otherCurrentliabilities.Add("type", "Other Current Liabilities");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherCurrentliabilities.Add(item.period, item.otherCurrentliabilities);
                            }
                            currentLiabilitiesInner.Add(otherCurrentliabilities);
                            currentLiabilities.Add("innerData", currentLiabilitiesInner);


                            Dictionary<string, object> totalLongTermDebt = new Dictionary<string, object>();
                            totalLongTermDebt.Add("type", "Total Long Term Debt");
                            totalLongTermDebt.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                totalLongTermDebt.Add(item.period, item.totalLongTermDebt);
                            }
                            totalLongTermDebt.Add("innerData", blankArray);

                            Dictionary<string, object> otherLiabilities = new Dictionary<string, object>();
                            otherLiabilities.Add("type", "Other Liabilities");
                            otherLiabilities.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherLiabilities.Add(item.period, item.otherLiabilities);
                            }
                            otherLiabilities.Add("innerData", blankArray);

                            Dictionary<string, object> deferredIncomeTax = new Dictionary<string, object>();
                            deferredIncomeTax.Add("type", "Deferred Income Tax");
                            deferredIncomeTax.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                deferredIncomeTax.Add(item.period, item.deferredIncomeTax);
                            }
                            deferredIncomeTax.Add("innerData", blankArray);


                            balanceSheet.Add(currentAssets);
                            balanceSheet.Add(intangiblesAssets);
                            balanceSheet.Add(LTAssets);
                            balanceSheet.Add(propertyPlantEquipment);
                            balanceSheet.Add(liabilitiesShareholdersEquity);

                            balanceSheet.Add(totalEquity);
                            balanceSheet.Add(currentLiabilities);
                            balanceSheet.Add(totalLongTermDebt);
                            balanceSheet.Add(otherLiabilities);
                            balanceSheet.Add(deferredIncomeTax);
                            balanceSheet.Add(totalLiabilities);
                            #endregion
                        }
                        else if (data.tableType == "cashflows")
                        {
                            #region cashflow

                            //cashFromFinancialAct start
                            Dictionary<string, object> cashFromFinancialAct = new Dictionary<string, object>();
                            cashFromFinancialAct.Add("type", "Cash from Financing Activity");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                cashFromFinancialAct.Add(item.period.ToString(), item.netCashFinancingActivities);
                            }

                            List<Dictionary<string, object>> innercashFromFinancialAct = new List<Dictionary<string, object>>();

                            Dictionary<string, object> cashDividendsPaid = new Dictionary<string, object>();
                            cashDividendsPaid.Add("type", "Cash Dividends Paid");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                cashDividendsPaid.Add(item.period.ToString(), item.cashDividendsPaid);
                            }
                            innercashFromFinancialAct.Add(cashDividendsPaid);

                            Dictionary<string, object> issuanceReductionCapitalStock = new Dictionary<string, object>();
                            issuanceReductionCapitalStock.Add("type", "Reduction in Capital Stock");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                issuanceReductionCapitalStock.Add(item.period.ToString(), item.issuanceReductionCapitalStock);
                            }
                            innercashFromFinancialAct.Add(issuanceReductionCapitalStock);

                            Dictionary<string, object> issuanceReductionDebtNet = new Dictionary<string, object>();
                            issuanceReductionDebtNet.Add("type", "Reduction in Debt Net");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                issuanceReductionDebtNet.Add(item.period.ToString(), item.issuanceReductionDebtNet);
                            }
                            innercashFromFinancialAct.Add(issuanceReductionDebtNet);

                            Dictionary<string, object> otherFundsFinancingItems = new Dictionary<string, object>();
                            otherFundsFinancingItems.Add("type", "Other Cash Financing Items");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                otherFundsFinancingItems.Add(item.period.ToString(), item.otherFundsFinancingItems);
                            }
                            innercashFromFinancialAct.Add(otherFundsFinancingItems);
                            cashFromFinancialAct.Add("innerData", innercashFromFinancialAct);
                            //cashFromFinancialAct end

                            //cashInvestAct start
                            Dictionary<string, object> cashInvestAct = new Dictionary<string, object>();
                            cashInvestAct.Add("type", "Cash from Investing Activity");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                cashInvestAct.Add(item.period.ToString(), item.netInvestingCashFlow);
                            }
                            List<Dictionary<string, object>> innercashInvestAct = new List<Dictionary<string, object>>();
                            Dictionary<string, object> capex = new Dictionary<string, object>();
                            capex.Add("type", "Capex");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                capex.Add(item.period, item.capex);
                            }
                            innercashInvestAct.Add(capex);

                            Dictionary<string, object> otherInvestingCashFlowItemsTotal = new Dictionary<string, object>();
                            otherInvestingCashFlowItemsTotal.Add("type", "Other Investing Cash Items");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                otherInvestingCashFlowItemsTotal.Add(item.period, item.otherInvestingCashFlowItemsTotal);
                            }
                            innercashInvestAct.Add(otherInvestingCashFlowItemsTotal);
                            cashInvestAct.Add("innerData", innercashInvestAct);
                            //cashInvestAct end

                            Dictionary<string, object> cashOperateAct = new Dictionary<string, object>();
                            cashOperateAct.Add("type", "Cash from Operating Activity");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                cashOperateAct.Add(item.period.ToString(), item.netOperatingCashFlow);
                            }

                            List<Dictionary<string, object>> innercashOperateAct = new List<Dictionary<string, object>>();
                            Dictionary<string, object> netIncomeStartingLine = new Dictionary<string, object>();
                            netIncomeStartingLine.Add("type", "Net Income");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                netIncomeStartingLine.Add(item.period, item.netIncomeStartingLine);
                            }
                            innercashOperateAct.Add(netIncomeStartingLine);

                            Dictionary<string, object> changesinWorkingCapital = new Dictionary<string, object>();
                            changesinWorkingCapital.Add("type", "Change In Working Capital");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                changesinWorkingCapital.Add(item.period, item.changesinWorkingCapital);
                            }
                            innercashOperateAct.Add(changesinWorkingCapital);

                            Dictionary<string, object> deferredTaxesInvestmentTaxCredit = new Dictionary<string, object>();
                            deferredTaxesInvestmentTaxCredit.Add("type", "Deferred Taxes Investment Tax Credit");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                deferredTaxesInvestmentTaxCredit.Add(item.period, item.deferredTaxesInvestmentTaxCredit);
                            }
                            innercashOperateAct.Add(deferredTaxesInvestmentTaxCredit);

                            Dictionary<string, object> depreciationAmortization = new Dictionary<string, object>();
                            depreciationAmortization.Add("type", "Depreciation & Amortization");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                depreciationAmortization.Add(item.period, item.depreciationAmortization);
                            }
                            innercashOperateAct.Add(depreciationAmortization);

                            Dictionary<string, object> otherFundsNonCashItems = new Dictionary<string, object>();
                            otherFundsNonCashItems.Add("type", "Other Funds Non Cash Items");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                otherFundsNonCashItems.Add(item.period, item.otherFundsNonCashItems);
                            }
                            innercashOperateAct.Add(otherFundsNonCashItems);
                            cashOperateAct.Add("innerData", innercashOperateAct);

                            cashflow.Add(cashFromFinancialAct);
                            cashflow.Add(cashInvestAct);
                            cashflow.Add(cashOperateAct);

                            #endregion
                        }
                        else
                        {
                            #region pnl


                            //revenue
                            Dictionary<string, object> revenue = new Dictionary<string, object>();
                            revenue.Add("type", "Revenue");
                            foreach (var item in res.ResultData.pnl)
                            {
                                revenue.Add(item.period.ToString(), item.revenue);
                            }
                            revenue.Add("innerData", blankArray);

                            //costOfGoodsSold
                            Dictionary<string, object> costOfGoodsSold = new Dictionary<string, object>();
                            costOfGoodsSold.Add("type", "Cost of Goods Sold");
                            foreach (var item in res.ResultData.pnl)
                            {
                                costOfGoodsSold.Add(item.period.ToString(), item.costOfGoodsSold);
                            }
                            costOfGoodsSold.Add("innerData", blankArray);

                            //groosProfiit
                            Dictionary<string, object> groosProfiit = new Dictionary<string, object>();
                            groosProfiit.Add("type", "Gross Profit");
                            foreach (var item in res.ResultData.pnl)
                            {
                                groosProfiit.Add(item.period.ToString(), item.grossIncome);
                            }
                            groosProfiit.Add("innerData", blankArray);

                            //groosProfiitPer
                            Dictionary<string, object> groosProfiitPer = new Dictionary<string, object>();
                            groosProfiitPer.Add("type", "Gross Profit %");
                            foreach (var item in res.ResultData.pnl)
                            {
                                groosProfiitPer.Add(item.period.ToString(), item.grossProfiltPer);
                            }
                            groosProfiitPer.Add("innerData", blankArray);


                            //expance
                            Dictionary<string, object> totalOperatingExpense = new Dictionary<string, object>();
                            totalOperatingExpense.Add("type", "Total Operating Expense");
                            foreach (var item in res.ResultData.pnl)
                            {
                                totalOperatingExpense.Add(item.period.ToString(), item.totalOperatingExpense);
                            }
                            //inner expance
                            List<Dictionary<string, object>> expanceInner = new List<Dictionary<string, object>>();
                            Dictionary<string, object> researchDevelopment = new Dictionary<string, object>();
                            researchDevelopment.Add("type", "Research Development");
                            foreach (var item in res.ResultData.pnl)
                            {
                                researchDevelopment.Add(item.period, item.researchDevelopment);
                            }
                            Dictionary<string, object> sgaExpense = new Dictionary<string, object>();
                            sgaExpense.Add("type", "SGA Expense");
                            foreach (var item in res.ResultData.pnl)
                            {
                                sgaExpense.Add(item.period.ToString(), item.sgaExpense);
                            }
                            expanceInner.Add(researchDevelopment);
                            expanceInner.Add(sgaExpense);
                            totalOperatingExpense.Add("innerData", expanceInner);

                            //EBIT
                            Dictionary<string, object> EBIT = new Dictionary<string, object>();
                            EBIT.Add("type", "EBIT");
                            foreach (var item in res.ResultData.pnl)
                            {
                                EBIT.Add(item.period.ToString(), item.ebit);
                            }
                            EBIT.Add("innerData", blankArray);

                            //EBITper
                            Dictionary<string, object> EBITper = new Dictionary<string, object>();
                            EBITper.Add("type", "EBIT %");
                            foreach (var item in res.ResultData.pnl)
                            {
                                EBITper.Add(item.period.ToString(), item.ebitPer);
                            }
                            EBITper.Add("innerData", blankArray);

                            //interestIncomeExpense
                            Dictionary<string, object> interestIncomeExpense = new Dictionary<string, object>();
                            interestIncomeExpense.Add("type", "Interest Income");
                            foreach (var item in res.ResultData.pnl)
                            {
                                interestIncomeExpense.Add(item.period.ToString(), item.interestIncomeExpense);
                            }
                            interestIncomeExpense.Add("innerData", blankArray);

                            //totalOtherIncomeExpenseNet
                            Dictionary<string, object> totalOtherIncomeExpenseNet = new Dictionary<string, object>();
                            totalOtherIncomeExpenseNet.Add("type", "Other Income");
                            foreach (var item in res.ResultData.pnl)
                            {
                                totalOtherIncomeExpenseNet.Add(item.period.ToString(), item.totalOtherIncomeExpenseNet);
                            }
                            totalOtherIncomeExpenseNet.Add("innerData", blankArray);

                            //provisionforIncomeTaxes
                            Dictionary<string, object> provisionforIncomeTaxes = new Dictionary<string, object>();
                            provisionforIncomeTaxes.Add("type", "Taxes");
                            foreach (var item in res.ResultData.pnl)
                            {
                                provisionforIncomeTaxes.Add(item.period.ToString(), item.provisionforIncomeTaxes);
                            }
                            provisionforIncomeTaxes.Add("innerData", blankArray);

                            //net income with inner data
                            Dictionary<string, object> netIncome = new Dictionary<string, object>();
                            netIncome.Add("type", "Net Income");
                            foreach (var item in res.ResultData.pnl)
                            {
                                netIncome.Add(item.period.ToString(), item.netIncome);
                            }
                            netIncome.Add("innerData", blankArray);
                            //innner net inconmer

                            //List<Dictionary<string, object>> netIncomeInner = new List<Dictionary<string, object>>();
                            //netIncomeInner.Add(EBIT);
                            //netIncomeInner.Add(interestIncomeExpense);
                            //netIncomeInner.Add(provisionforIncomeTaxes);
                            //netIncomeInner.Add(totalOtherIncomeExpenseNet);
                            //netIncome.Add("innerData", netIncomeInner);

                            //EPS income with inner data
                            Dictionary<string, object> EPS = new Dictionary<string, object>();
                            EPS.Add("type", "EPS");
                            foreach (var item in res.ResultData.pnl)
                            {
                                EPS.Add(item.period.ToString(), item.dilutedEPS);
                            }
                            EPS.Add("innerData", blankArray);

                            //List<Dictionary<string, object>> EPSInner = new List<Dictionary<string, object>>();
                            //Dictionary<string, object> netIncome1 = new Dictionary<string, object>();
                            //netIncome1.Add("type", "Net Income");
                            //foreach (var item in res.ResultData.pnl)
                            //{
                            //    netIncome1.Add(item.period.ToString(), item.netIncome);
                            //}
                            //EPSInner.Add(netIncome1);
                            Dictionary<string, object> dilutedAverageSharesOutstanding = new Dictionary<string, object>();
                            dilutedAverageSharesOutstanding.Add("type", "Average Share Outstanding");
                            foreach (var item in res.ResultData.pnl)
                            {
                                dilutedAverageSharesOutstanding.Add(item.period.ToString(), item.dilutedAverageSharesOutstanding);
                            }
                            dilutedAverageSharesOutstanding.Add("innerData", blankArray);
                            //EPSInner.Add(dilutedAverageSharesOutstanding);
                            //EPS.Add("innerData", EPSInner);

                            pnl.Add(revenue);
                            pnl.Add(costOfGoodsSold);
                            pnl.Add(groosProfiit);
                            pnl.Add(groosProfiitPer);
                            pnl.Add(totalOperatingExpense);
                            pnl.Add(EBIT);
                            pnl.Add(EBITper);
                            pnl.Add(interestIncomeExpense);
                            pnl.Add(totalOtherIncomeExpenseNet);
                            pnl.Add(provisionforIncomeTaxes);
                            pnl.Add(netIncome);
                            pnl.Add(dilutedAverageSharesOutstanding);
                            pnl.Add(EPS);

                            #endregion


                            #region balanceSheet

                            //blankArray

                            //total assest
                            Dictionary<string, object> liabilitiesShareholdersEquity = new Dictionary<string, object>();
                            liabilitiesShareholdersEquity.Add("type", "Total Assets");
                            liabilitiesShareholdersEquity.Add("isTotal", 1);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                liabilitiesShareholdersEquity.Add(item.period.ToString(), item.liabilitiesShareholdersEquity);
                            }
                            liabilitiesShareholdersEquity.Add("innerData", blankArray);

                            //Current Assets with inner data
                            Dictionary<string, object> currentAssets = new Dictionary<string, object>();
                            currentAssets.Add("type", "Current Assets");
                            currentAssets.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                currentAssets.Add(item.period.ToString(), item.currentAssets);
                            }

                            List<Dictionary<string, object>> currentAssetsInner = new List<Dictionary<string, object>>();

                            Dictionary<string, object> cashShortTermInvestments = new Dictionary<string, object>();
                            cashShortTermInvestments.Add("type", "Cash Short Term Investments");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                cashShortTermInvestments.Add(item.period, item.cashShortTermInvestments);
                            }
                            currentAssetsInner.Add(cashShortTermInvestments);

                            Dictionary<string, object> cash = new Dictionary<string, object>();
                            cash.Add("type", "Cash");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                cash.Add(item.period, item.cash);
                            }
                            currentAssetsInner.Add(cash);

                            Dictionary<string, object> cashEquivalents = new Dictionary<string, object>();
                            cashEquivalents.Add("type", "Cash Equivalents");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                cashEquivalents.Add(item.period, item.cashEquivalents);
                            }
                            currentAssetsInner.Add(cashEquivalents);


                            Dictionary<string, object> shortTermInvestments = new Dictionary<string, object>();
                            shortTermInvestments.Add("type", "Short Term Investments");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                shortTermInvestments.Add(item.period, item.shortTermInvestments);
                            }
                            currentAssetsInner.Add(shortTermInvestments);

                            Dictionary<string, object> totalReceivables = new Dictionary<string, object>();
                            totalReceivables.Add("type", "A/C Receivables");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                totalReceivables.Add(item.period, item.totalReceivables);
                            }
                            currentAssetsInner.Add(totalReceivables);

                            Dictionary<string, object> inventory = new Dictionary<string, object>();
                            inventory.Add("type", "Inventory");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                inventory.Add(item.period, item.inventory);
                            }
                            currentAssetsInner.Add(inventory);

                            Dictionary<string, object> otherCurrentAssets = new Dictionary<string, object>();
                            otherCurrentAssets.Add("type", "Other Current Assets");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherCurrentAssets.Add(item.period, item.otherCurrentAssets);
                            }
                            currentAssetsInner.Add(otherCurrentAssets);

                            Dictionary<string, object> otherReceivables = new Dictionary<string, object>();
                            otherReceivables.Add("type", "Other Receivables");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherReceivables.Add(item.period, item.otherReceivables);
                            }
                            currentAssetsInner.Add(otherReceivables);

                            currentAssets.Add("innerData", currentAssetsInner);
                            //Current Assets with inner data end


                            //Intangibles with inner data
                            Dictionary<string, object> intangiblesAssets = new Dictionary<string, object>();
                            intangiblesAssets.Add("type", "Intangibles Assets");
                            intangiblesAssets.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                intangiblesAssets.Add(item.period, item.goodwill + item.intangiblesAssets);
                            }

                            List<Dictionary<string, object>> intangiblesAssetsInner = new List<Dictionary<string, object>>();
                            Dictionary<string, object> goodwill = new Dictionary<string, object>();
                            goodwill.Add("type", "Goodwill");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                goodwill.Add(item.period, item.goodwill);
                            }
                            intangiblesAssetsInner.Add(goodwill);

                            Dictionary<string, object> otherintangiblesAssets = new Dictionary<string, object>();
                            otherintangiblesAssets.Add("type", "Other Intangibles Assets");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherintangiblesAssets.Add(item.period, item.intangiblesAssets);
                            }
                            intangiblesAssetsInner.Add(otherintangiblesAssets);

                            intangiblesAssets.Add("innerData", intangiblesAssetsInner);
                            //Intangibles with inner data end

                            //LT with inner data
                            Dictionary<string, object> LTAssets = new Dictionary<string, object>();
                            LTAssets.Add("type", "LT Assets");
                            LTAssets.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                LTAssets.Add(item.period, item.noteReceivableLongTerm + item.longTermInvestments + item.otherLongTermAssets);
                            }

                            List<Dictionary<string, object>> LTAssetsInner = new List<Dictionary<string, object>>();

                            Dictionary<string, object> noteReceivableLongTerm = new Dictionary<string, object>();
                            noteReceivableLongTerm.Add("type", "Note Receivable Long Term");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                noteReceivableLongTerm.Add(item.period, item.noteReceivableLongTerm);
                            }
                            LTAssetsInner.Add(noteReceivableLongTerm);

                            Dictionary<string, object> longTermInvestments = new Dictionary<string, object>();
                            longTermInvestments.Add("type", "Long Term Investments");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                longTermInvestments.Add(item.period, item.longTermInvestments);
                            }
                            LTAssetsInner.Add(longTermInvestments);

                            Dictionary<string, object> otherLongTermAssets = new Dictionary<string, object>();
                            otherLongTermAssets.Add("type", "Other Long Term Assets");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherLongTermAssets.Add(item.period, item.otherLongTermAssets);
                            }
                            LTAssetsInner.Add(otherLongTermAssets);
                            LTAssets.Add("innerData", LTAssetsInner);

                            //LT with inner dataend

                            //propertyPlantEquipment
                            Dictionary<string, object> propertyPlantEquipment = new Dictionary<string, object>();
                            propertyPlantEquipment.Add("type", "Property Plant Equipment");
                            propertyPlantEquipment.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                propertyPlantEquipment.Add(item.period, item.propertyPlantEquipment);
                            }
                            propertyPlantEquipment.Add("innerData", blankArray);


                            //totalLiabilities
                            Dictionary<string, object> totalLiabilities = new Dictionary<string, object>();
                            totalLiabilities.Add("type", "Total Liabilities");
                            totalLiabilities.Add("isTotal", 1);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                totalLiabilities.Add(item.period, item.totalLiabilities);
                            }
                            totalLiabilities.Add("innerData", blankArray);

                            //equity with inner data
                            Dictionary<string, object> totalEquity = new Dictionary<string, object>();
                            totalEquity.Add("type", "Total Equity");
                            totalEquity.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                totalEquity.Add(item.period, item.totalEquity);
                            }

                            List<Dictionary<string, object>> totalEquityInner = new List<Dictionary<string, object>>();

                            Dictionary<string, object> commonStock = new Dictionary<string, object>();
                            commonStock.Add("type", "Common Stock");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                commonStock.Add(item.period, item.commonStock);
                            }
                            totalEquityInner.Add(commonStock);

                            Dictionary<string, object> otherEquity = new Dictionary<string, object>();
                            otherEquity.Add("type", "Other Equity");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherEquity.Add(item.period, item.otherEquity);
                            }
                            totalEquityInner.Add(otherEquity);

                            Dictionary<string, object> retainedEarnings = new Dictionary<string, object>();
                            retainedEarnings.Add("type", "Retained Earning");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                retainedEarnings.Add(item.period, item.retainedEarnings);
                            }
                            totalEquityInner.Add(retainedEarnings);

                            Dictionary<string, object> unrealizedProfitLossSecurity = new Dictionary<string, object>();
                            unrealizedProfitLossSecurity.Add("type", "Unrealized Profit Loss Security");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                unrealizedProfitLossSecurity.Add(item.period, item.unrealizedProfitLossSecurity);
                            }
                            totalEquityInner.Add(unrealizedProfitLossSecurity);

                            Dictionary<string, object> miscellaneous = new Dictionary<string, object>();
                            miscellaneous.Add("type", "Miscellaneous");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                miscellaneous.Add(item.period, item.miscellaneous);
                            }
                            totalEquityInner.Add(miscellaneous);
                            totalEquity.Add("innerData", totalEquityInner);
                            //totalEquity  end


                            //equity with inner data
                            Dictionary<string, object> currentLiabilities = new Dictionary<string, object>();
                            currentLiabilities.Add("type", "Current Liabilities");
                            currentLiabilities.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                currentLiabilities.Add(item.period, item.currentLiabilities);
                            }

                            List<Dictionary<string, object>> currentLiabilitiesInner = new List<Dictionary<string, object>>();

                            Dictionary<string, object> accountsPayable = new Dictionary<string, object>();
                            accountsPayable.Add("type", "Accounts Payable");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                accountsPayable.Add(item.period, item.accountsPayable);
                            }
                            currentLiabilitiesInner.Add(accountsPayable);


                            Dictionary<string, object> accruedLiability = new Dictionary<string, object>();
                            accruedLiability.Add("type", "Accrued Liability");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                accruedLiability.Add(item.period, item.accruedLiability);
                            }
                            currentLiabilitiesInner.Add(accruedLiability);

                            Dictionary<string, object> currentPortionLongTermDebt = new Dictionary<string, object>();
                            currentPortionLongTermDebt.Add("type", "Current Portion Long Term Debt");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                currentPortionLongTermDebt.Add(item.period, item.currentPortionLongTermDebt);
                            }
                            currentLiabilitiesInner.Add(currentPortionLongTermDebt);

                            Dictionary<string, object> shortTermDebt = new Dictionary<string, object>();
                            shortTermDebt.Add("type", "Short Term Debt");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                shortTermDebt.Add(item.period, item.shortTermDebt);
                            }
                            currentLiabilitiesInner.Add(shortTermDebt);

                            Dictionary<string, object> otherCurrentliabilities = new Dictionary<string, object>();
                            otherCurrentliabilities.Add("type", "Other Current Liabilities");
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherCurrentliabilities.Add(item.period, item.otherCurrentliabilities);
                            }
                            currentLiabilitiesInner.Add(otherCurrentliabilities);
                            currentLiabilities.Add("innerData", currentLiabilitiesInner);


                            Dictionary<string, object> totalLongTermDebt = new Dictionary<string, object>();
                            totalLongTermDebt.Add("type", "Total Long Term Debt");
                            totalLongTermDebt.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                totalLongTermDebt.Add(item.period, item.totalLongTermDebt);
                            }
                            totalLongTermDebt.Add("innerData", blankArray);

                            Dictionary<string, object> otherLiabilities = new Dictionary<string, object>();
                            otherLiabilities.Add("type", "Other Liabilities");
                            otherLiabilities.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                otherLiabilities.Add(item.period, item.otherLiabilities);
                            }
                            otherLiabilities.Add("innerData", blankArray);

                            Dictionary<string, object> deferredIncomeTax = new Dictionary<string, object>();
                            deferredIncomeTax.Add("type", "Deferred Income Tax");
                            deferredIncomeTax.Add("isTotal", 0);
                            foreach (var item in res.ResultData.balanceShhet)
                            {
                                deferredIncomeTax.Add(item.period, item.deferredIncomeTax);
                            }
                            deferredIncomeTax.Add("innerData", blankArray);


                            balanceSheet.Add(currentAssets);
                            balanceSheet.Add(intangiblesAssets);
                            balanceSheet.Add(LTAssets);
                            balanceSheet.Add(propertyPlantEquipment);
                            balanceSheet.Add(liabilitiesShareholdersEquity);

                            balanceSheet.Add(totalEquity);
                            balanceSheet.Add(currentLiabilities);
                            balanceSheet.Add(totalLongTermDebt);
                            balanceSheet.Add(otherLiabilities);
                            balanceSheet.Add(deferredIncomeTax);
                            balanceSheet.Add(totalLiabilities);
                            #endregion


                            #region cashflow

                            //cashFromFinancialAct start
                            Dictionary<string, object> cashFromFinancialAct = new Dictionary<string, object>();
                            cashFromFinancialAct.Add("type", "Cash from Financing Activity");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                cashFromFinancialAct.Add(item.period.ToString(), item.netCashFinancingActivities);
                            }

                            List<Dictionary<string, object>> innercashFromFinancialAct = new List<Dictionary<string, object>>();

                            Dictionary<string, object> cashDividendsPaid = new Dictionary<string, object>();
                            cashDividendsPaid.Add("type", "Cash Dividends Paid");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                cashDividendsPaid.Add(item.period.ToString(), item.cashDividendsPaid);
                            }
                            innercashFromFinancialAct.Add(cashDividendsPaid);

                            Dictionary<string, object> issuanceReductionCapitalStock = new Dictionary<string, object>();
                            issuanceReductionCapitalStock.Add("type", "Reduction in Capital Stock");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                issuanceReductionCapitalStock.Add(item.period.ToString(), item.issuanceReductionCapitalStock);
                            }
                            innercashFromFinancialAct.Add(issuanceReductionCapitalStock);

                            Dictionary<string, object> issuanceReductionDebtNet = new Dictionary<string, object>();
                            issuanceReductionDebtNet.Add("type", "Reduction in Debt Net");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                issuanceReductionDebtNet.Add(item.period.ToString(), item.issuanceReductionDebtNet);
                            }
                            innercashFromFinancialAct.Add(issuanceReductionDebtNet);

                            Dictionary<string, object> otherFundsFinancingItems = new Dictionary<string, object>();
                            otherFundsFinancingItems.Add("type", "Other Cash Financing Items");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                otherFundsFinancingItems.Add(item.period.ToString(), item.otherFundsFinancingItems);
                            }
                            innercashFromFinancialAct.Add(otherFundsFinancingItems);
                            cashFromFinancialAct.Add("innerData", innercashFromFinancialAct);
                            //cashFromFinancialAct end

                            //cashInvestAct start
                            Dictionary<string, object> cashInvestAct = new Dictionary<string, object>();
                            cashInvestAct.Add("type", "Cash from Investing Activity");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                cashInvestAct.Add(item.period.ToString(), item.netInvestingCashFlow);
                            }
                            List<Dictionary<string, object>> innercashInvestAct = new List<Dictionary<string, object>>();
                            Dictionary<string, object> capex = new Dictionary<string, object>();
                            capex.Add("type", "Capex");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                capex.Add(item.period, item.capex);
                            }
                            innercashInvestAct.Add(capex);

                            Dictionary<string, object> otherInvestingCashFlowItemsTotal = new Dictionary<string, object>();
                            otherInvestingCashFlowItemsTotal.Add("type", "Other Investing Cash Items");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                otherInvestingCashFlowItemsTotal.Add(item.period, item.otherInvestingCashFlowItemsTotal);
                            }
                            innercashInvestAct.Add(otherInvestingCashFlowItemsTotal);
                            cashInvestAct.Add("innerData", innercashInvestAct);
                            //cashInvestAct end

                            Dictionary<string, object> cashOperateAct = new Dictionary<string, object>();
                            cashOperateAct.Add("type", "Cash from Operating Activity");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                cashOperateAct.Add(item.period.ToString(), item.netOperatingCashFlow);
                            }

                            List<Dictionary<string, object>> innercashOperateAct = new List<Dictionary<string, object>>();
                            Dictionary<string, object> netIncomeStartingLine = new Dictionary<string, object>();
                            netIncomeStartingLine.Add("type", "Net Income");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                netIncomeStartingLine.Add(item.period, item.netIncomeStartingLine);
                            }
                            innercashOperateAct.Add(netIncomeStartingLine);

                            Dictionary<string, object> changesinWorkingCapital = new Dictionary<string, object>();
                            changesinWorkingCapital.Add("type", "Change In Working Capital");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                changesinWorkingCapital.Add(item.period, item.changesinWorkingCapital);
                            }
                            innercashOperateAct.Add(changesinWorkingCapital);

                            Dictionary<string, object> deferredTaxesInvestmentTaxCredit = new Dictionary<string, object>();
                            deferredTaxesInvestmentTaxCredit.Add("type", "Deferred Taxes Investment Tax Credit");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                deferredTaxesInvestmentTaxCredit.Add(item.period, item.deferredTaxesInvestmentTaxCredit);
                            }
                            innercashOperateAct.Add(deferredTaxesInvestmentTaxCredit);

                            Dictionary<string, object> depreciationAmortization = new Dictionary<string, object>();
                            depreciationAmortization.Add("type", "Depreciation & Amortization");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                depreciationAmortization.Add(item.period, item.depreciationAmortization);
                            }
                            innercashOperateAct.Add(depreciationAmortization);

                            Dictionary<string, object> otherFundsNonCashItems = new Dictionary<string, object>();
                            otherFundsNonCashItems.Add("type", "Other Funds Non Cash Items");
                            foreach (var item in res.ResultData.cashFlow)
                            {
                                otherFundsNonCashItems.Add(item.period, item.otherFundsNonCashItems);
                            }
                            innercashOperateAct.Add(otherFundsNonCashItems);
                            cashOperateAct.Add("innerData", innercashOperateAct);

                            cashflow.Add(cashFromFinancialAct);
                            cashflow.Add(cashInvestAct);
                            cashflow.Add(cashOperateAct);

                            #endregion
                        }

                        if (res.ResultData.description.address == "not availabe")
                        {
                            return new
                            {
                                result = 1,
                                message = "Success",
                                stockAvailable = 0,
                                data = new
                                {
                                    res.ResultData.description
                                }
                            } as dynamic;
                        }
                        else
                        {
                            ResultObject<List<dynamic>> resPeer = new ResultObject<List<dynamic>>();
                            resPeer = _IStocksApiMgr.getData("getpeer", data.symbol, "");

                            if (data.tableType == "pnl")
                            {
                                return new
                                {
                                    result = 1,
                                    message = "Success",
                                    stockAvailable = 1,
                                    data = new
                                    {
                                        res.ResultData.description,
                                        pnl = pnl
                                    }
                                } as dynamic;
                            }
                            else if (data.tableType == "balsheet")
                            {
                                return new
                                {
                                    result = 1,
                                    message = "Success",
                                    stockAvailable = 1,
                                    data = new
                                    {
                                        res.ResultData.description,
                                        balanceSheet = balanceSheet,
                                    }
                                } as dynamic;
                            }
                            else if (data.tableType == "cashflows")
                            {
                                return new
                                {
                                    result = 1,
                                    message = "Success",
                                    stockAvailable = 1,
                                    data = new
                                    {
                                        res.ResultData.description,
                                        cashflow = cashflow
                                    }
                                } as dynamic;
                            }
                            else
                            {
                                return new
                                {
                                    result = 1,
                                    message = "Success",
                                    stockAvailable = 1,
                                    data = new
                                    {
                                        res.ResultData.description,
                                        pnl = pnl,
                                        balanceSheet = balanceSheet,
                                        cashflow = cashflow,
                                        peers = resPeer.ResultData
                                    }
                                } as dynamic;
                            }


                        }


                    }
                    else if (res.Result == ResultType.Info)
                    {
                        return new
                        {
                            result = 0,
                            message = "no analysis available, please contact administrator"
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 0,
                            message = "Invalid Data"
                        } as dynamic;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getStockData", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }

        [HttpPost]
        public dynamic company_news(stockPost data)
        {
            ResultObject<List<News>> res = new ResultObject<List<News>>();
            try
            {
                res = new ResultObject<List<News>>();
                var BaseURL = _iConfig.GetConnectionString("BaseURL");
                var ApiKey = _iConfig.GetConnectionString("ApiKey");

                data.fromDate = DateTime.Now.Date.AddDays(-3).ToString("yyyy-MM-dd");
                if (data.type == "all")
                {
                    data.fromDate = DateTime.Now.Date.AddDays(-7).ToString("yyyy-MM-dd");
                }
                data.toDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                string requestURL = BaseURL + "v1" + "/company-news?symbol=" + data.symbol + "&from=" + data.fromDate + "&to=" + data.toDate;

                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", ApiKey);

                var json = client.DownloadString(requestURL);
                List<News> j = JsonConvert.DeserializeObject<List<News>>(json);
                j = j.Select(s => { s.datetime = BasicMethod.UnixTimeStampToDateTime(double.Parse(s.datetime)).AddHours(5.30).ToString(); s.summary = "" + s.summary + ""; s.headline = BasicMethod.RemoveDiacritics(s.headline); return s; }).ToList();
                client.Dispose();
                return new
                {
                    result = 1,
                    message = "Success",
                    data = j
                } as dynamic;
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = new string[] { }
                } as dynamic;
            }
            // return j;
        }


        [HttpPost]
        public dynamic pressRelease(stockPost data)
        {
            ResultObject<List<Pressrelease>> res = new ResultObject<List<Pressrelease>>();
            try
            {
                res = new ResultObject<List<Pressrelease>>();
                var BaseURL = _iConfig.GetConnectionString("BaseURL");
                var ApiKey = _iConfig.GetConnectionString("ApiKey");

                var today = DateTime.Today;
                var month = new DateTime(today.Year, today.Month, 1);
                var first = month.AddMonths(-1);

                data.fromDate = first.ToString("yyyy-MM-dd");
                data.toDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                string requestURL = "";
                requestURL = BaseURL + "v1" + "/press-releases?symbol=" + data.symbol + "&from=" + data.fromDate + "&to=" + data.toDate;
                if (data.type == "all")
                {
                    requestURL = BaseURL + "v1" + "/press-releases?symbol=" + data.symbol;
                }

                WebClient client = new WebClient();
                client.Headers.Set("X-Finnhub-Token", ApiKey);

                var json = client.DownloadString(requestURL);
                Pressrelease j = JsonConvert.DeserializeObject<Pressrelease>(json);
                j.majorDevelopment = j.majorDevelopment.Select(s => { s.headline = BasicMethod.RemoveDiacritics(s.headline); s.description = BasicMethod.RemoveDiacritics(s.description); return s; }).ToList();
                return new
                {
                    result = 1,
                    message = "Success",
                    data = j.majorDevelopment
                } as dynamic;
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = new string[] { }
                } as dynamic;
            }
            // return j;
        }

        #region USP_Stock_OTHER
        [HttpPost]
        public dynamic getCandleDaily(stockPost data)
        {
            ResultObject<List<dynamic>> res = new ResultObject<List<dynamic>>();
            try
            {
                if (data.symbol == "")
                {
                    return new
                    {
                        result = 0,
                        message = "Please Enter Symbol"
                    } as dynamic;
                }
                else
                {
                    if (data.dataType == "")
                    {
                        data.dataType = "daily";
                    }
                    res = _IStocksApiMgr.getData("getCandle", data.symbol, data.dataType);
                }
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getCandleDaily", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }


        [HttpPost]
        public dynamic getPriceTargetList(stockPost data)
        {
            ResultObject<List<dynamic>> res = new ResultObject<List<dynamic>>();
            try
            {
                if (data.symbol == "")
                {
                    return new
                    {
                        result = 0,
                        message = "Please Enter Symbol"
                    } as dynamic;
                }
                else
                {
                    res = _IStocksApiMgr.getData("getpricetarget", data.symbol, "");
                }
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.FirstOrDefault()
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getPriceTargetList", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }

        [HttpPost]
        public dynamic getDividend(stockPost data)
        {
            ResultObject<List<dynamic>> res = new ResultObject<List<dynamic>>();
            try
            {
                if (data.symbol == "")
                {
                    return new
                    {
                        result = 0,
                        message = "Please Enter Symbol"
                    } as dynamic;
                }
                else
                {
                    res = _IStocksApiMgr.getData("getDividend", data.symbol, "");
                }
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "No Record Found"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getDividend", "StockPost", "getDividend", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }

        [HttpPost]
        public dynamic getSECFilings(stockPost data)
        {
            ResultObject<List<dynamic>> res = new ResultObject<List<dynamic>>();
            try
            {
                if (data.symbol == "")
                {
                    return new
                    {
                        result = 0,
                        message = "Please Enter Symbol"
                    } as dynamic;
                }
                else
                {
                    res = _IStocksApiMgr.getData("getSECFilings", data.symbol, "");
                }
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "No Record Found"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getDividend", "StockPost", "getDividend", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }

        [HttpPost]
        public dynamic getOwnership(stockPost data)
        {
            ResultObject<List<dynamic>> res = new ResultObject<List<dynamic>>();
            try
            {
                if (data.symbol == "")
                {
                    return new
                    {
                        result = 0,
                        message = "Please Enter Symbol"
                    } as dynamic;
                }
                else
                {
                    res = _IStocksApiMgr.getData("getOwnership", data.symbol, "");
                }
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "No Record Found"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getDividend", "StockPost", "getDividend", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }

        [HttpPost]
        public dynamic getQuoteList(stockPost data)
        {
            ResultObject<List<dynamic>> res = new ResultObject<List<dynamic>>();
            try
            {
                if (data.symbol == "")
                {
                    return new
                    {
                        result = 0,
                        message = "Please Enter Symbol"
                    } as dynamic;
                }
                else
                {
                    res = _IStocksApiMgr.getData("getquote", data.symbol, "");
                }
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.FirstOrDefault()
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getQuoteList", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }


        [HttpPost]
        public dynamic getStockMetaData(stockPost data)
        {
            ResultObject<List<dynamic>> res = new ResultObject<List<dynamic>>();
            try
            {
                if (data.symbol == "")
                {
                    return new
                    {
                        result = 0,
                        message = "Please Enter Symbol"
                    } as dynamic;
                }
                else
                {
                    res = _IStocksApiMgr.getData("getStockMetaData", data.symbol, "");
                }
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.FirstOrDefault()
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getQuoteList", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }
        #endregion

        [HttpPost]
        public dynamic getPeersData(stockPost data)
        {
            var idnetity = HttpContext.User.Identity as ClaimsIdentity;
            IList<Claim> claim = idnetity.Claims.ToList();
            string userid = "";
            if (claim.Count > 0)
            {
                userid = claim[0].Value;
            }


            ResultObject<DynamicList> res = new ResultObject<DynamicList>();
            try
            {
                if (data.symbol == "")
                {
                    return new
                    {
                        result = 0,
                        message = "Please Enter Symbol"
                    } as dynamic;
                }
                else
                {
                    res = _IStocksApiMgr.getPeer("getpeeredit", data.symbol, userid);
                }
                if (res.Result == ResultType.Success)
                {
                    var aa = res.ResultData.dynamics6.FirstOrDefault();
                    string b = "";
                    try
                    {
                        b = aa.aa.ToString();
                    }
                    catch (Exception ex)
                    {

                    }

                    if (b == "not exist")
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = new
                            {
                                peerData = res.ResultData.dynamics,
                                filterData = new
                                {
                                    financial = res.ResultData.dynamics1.FirstOrDefault(),
                                    priceAction = res.ResultData.dynamics2.FirstOrDefault(),
                                    historical = res.ResultData.dynamics3.FirstOrDefault(),
                                    ratios = res.ResultData.dynamics4.FirstOrDefault()
                                },
                                defaultColumn = res.ResultData.dynamics5.FirstOrDefault(),
                                savedColumn = new { }
                            }
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = new
                            {
                                peerData = res.ResultData.dynamics,
                                filterData = new
                                {
                                    financial = res.ResultData.dynamics1.FirstOrDefault(),
                                    priceAction = res.ResultData.dynamics2.FirstOrDefault(),
                                    historical = res.ResultData.dynamics3.FirstOrDefault(),
                                    ratios = res.ResultData.dynamics4.FirstOrDefault()
                                },
                                defaultColumn = res.ResultData.dynamics5.FirstOrDefault(),
                                savedColumn = res.ResultData.dynamics6.FirstOrDefault(),
                            }
                        } as dynamic;
                    }


                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "No Record Found"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getPeersData", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }

        [HttpPost]
        [Authorize]
        public dynamic savePeerColumn(peerPost data)
        {
            var idnetity = HttpContext.User.Identity as ClaimsIdentity;
            IList<Claim> claim = idnetity.Claims.ToList();
            var userid = claim[0].Value;
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IStocksApiMgr.updatedetails("updatepeeredit", userid.ToString(), "", data.peerColumns);
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success"
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("savePeerColumn", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }


        [HttpPost]
        [Authorize]
        public dynamic saveUserOrderDetails(userOrderPostData data)
        {
            var idnetity = HttpContext.User.Identity as ClaimsIdentity;
            IList<Claim> claim = idnetity.Claims.ToList();
            var userid = claim[0].Value;
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                data.txn_payment_status = "1";
                data.order_plans_json = "";
                var XMLData = XmlConversion.SerializeToXElement(data);
                res = _IStocksApiMgr.savedetails("saveurserorder", userid.ToString(), XMLData.ToString(), "", "USP_AdminUserOrder");
                if (res.Result == ResultType.Success)
                {

                    ResultObject<Usersdetails> res1 = new ResultObject<Usersdetails>();
                    Usersdetails data1 = new Usersdetails();
                    data1.email = userid.ToString();
                    res1 = _IStocksApiMgr.signup("loginorder", data1);
                    var tokenStr = GenerateJSONWebToken(res1.ResultData);
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            token = tokenStr,
                            res1.ResultData.mobile,
                            res1.ResultData.first_name,
                            res1.ResultData.last_name,
                            res1.ResultData.email,
                            res1.ResultData.email_is_verified,
                            res1.ResultData.status,
                            res1.ResultData.last_login,
                            res1.ResultData.created_at,
                            res1.ResultData.updated_at,
                            res1.ResultData.user_id,
                            res1.ResultData.membership_flag,
                            res1.ResultData.plan_type
                        }
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("savePeerColumn", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }

        //29-01
        [HttpPost]
        public dynamic getScreenerData(stockPost data)
        {
            var idnetity = HttpContext.User.Identity as ClaimsIdentity;
            IList<Claim> claim = idnetity.Claims.ToList();
            string userid = "";
            if (claim.Count > 0)
            {
                userid = claim[0].Value;
            }
            ResultObject<DynamicList> res = new ResultObject<DynamicList>();
            try
            {
                if (data.symbol == "")
                {
                    return new
                    {
                        result = 0,
                        message = "Please Enter Symbol"
                    } as dynamic;
                }
                else
                {
                    res = _IStocksApiMgr.getDynamic5("getscreener", data.symbol, userid, "USP_Stock_Screener");
                }
                if (res.Result == ResultType.Success)
                {
                    var aa = res.ResultData.dynamics5.FirstOrDefault();
                    string b = "";
                    try
                    {
                        b = aa.aa.ToString();
                    }
                    catch (Exception ex)
                    { }

                    if (b == "not exist")
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = new
                            {
                                screenerData = res.ResultData.dynamics,
                                filterData = new
                                {
                                    financial = res.ResultData.dynamics1.FirstOrDefault(),
                                    priceAction = res.ResultData.dynamics2.FirstOrDefault(),
                                    ratios = res.ResultData.dynamics3.FirstOrDefault()
                                },
                                defaultColumn = res.ResultData.dynamics4.FirstOrDefault(),
                                savedColumn = new { }
                            }
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = new
                            {
                                screenerData = res.ResultData.dynamics,
                                filterData = new
                                {
                                    financial = res.ResultData.dynamics1.FirstOrDefault(),
                                    priceAction = res.ResultData.dynamics2.FirstOrDefault(),
                                    ratios = res.ResultData.dynamics3.FirstOrDefault()
                                },
                                defaultColumn = res.ResultData.dynamics4.FirstOrDefault(),
                                savedColumn = res.ResultData.dynamics5.FirstOrDefault(),
                            }
                        } as dynamic;
                    }


                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "No Record Found"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getPeersData", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }

        [HttpPost]
        [Authorize]
        public dynamic saveScreenerColumn(peerPost data)
        {
            var idnetity = HttpContext.User.Identity as ClaimsIdentity;
            IList<Claim> claim = idnetity.Claims.ToList();
            var userid = claim[0].Value;
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IStocksApiMgr.updatedetails("updatescreener", userid.ToString(), "", data.screenerColumns);
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success"
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("savePeerColumn", "StockPost", "StockPost", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }
        private string GenerateJSONWebToken(Usersdetails userdata)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_iConfig["Jwt:Key"]));
            var credintials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Sub, userdata.user_id.ToString()),
                //new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Email, userdata.email),
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(
                issuer: _iConfig["Jwt:Issuer"],
                audience: _iConfig["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddDays(30),
                signingCredentials: credintials
                );
            var encodetoken = new JwtSecurityTokenHandler().WriteToken(token);
            return encodetoken;
        }
    }
}
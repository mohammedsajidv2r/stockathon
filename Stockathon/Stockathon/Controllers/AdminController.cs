﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SA_BaseObjet;
using SA_BaseObjet.Admin;
using SA_BaseObjet.StocksApi;
using SA_Mgr_Interface.Admin;
using SA_Utilities.Enum;

namespace Stockathon.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AdminController : Controller
    {
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IAdminMgr _IAdminMgr;
        IUserOrderMgr _IUserOrderMgr;

        public AdminController(IConfiguration iConfig, IHttpContextAccessor httpContextAccessor, IAdminMgr IAdminMgr, IUserOrderMgr IUserOrderMgr)
        {
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IAdminMgr = IAdminMgr;
            _IUserOrderMgr = IUserOrderMgr;
        }

        [HttpPost]
        public IActionResult SymbolListFilter()
        {
            FilterData data = new FilterData();
            data.sort = "symbol asc";
            int start = Convert.ToInt32(Request.Form["start"]);
            int length = Convert.ToInt32(Request.Form["length"]);
            if (length == 5)
            {
                start = start / 5;
            }
            else if (length == 10)
            {
                start = start / 10;
            }
            if (length == 25)
            {
                start = start / 25;
            }
            if (length == 50)
            {
                start = start / 50;
            }
            if (length == 100)
            {
                start = start / 100;
            }
            string searchValue  = Request.Form["search[value]"];
            string sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"] + "][data]"];
            string sortDirection = Request.Form["order[0][dir]"];
            data.sort = sortColumn + " " + sortDirection;
            data.start = start.ToString();
            data.length = length.ToString();
            ResultObject<List<Tbl_Stock_Symbol>> res = new ResultObject<List<Tbl_Stock_Symbol>>();

            if (searchValue == "")
            {
                res = _IAdminMgr.getStocks("symbollistpagination", "","",data);
            }
            else if (searchValue != "")
            {
                res = _IAdminMgr.getStocks("symbollistSerach", searchValue, "", data);
            }

            if (searchValue != "")
            {
                return Json(new
                {
                    data = res.ResultData.Skip(start).Take(length),
                    draw = Request.Form["draw"],
                    recordsTotal = res.ResultData.Select(x => x.displaySymbol).FirstOrDefault(),
                    recordsFiltered = res.ResultData.Select(x => x.displaySymbol).FirstOrDefault(),
                });
            }
            else
            {
                return Json(new
                {
                    data = res.ResultData,
                    draw = Request.Form["draw"],
                    recordsTotal = res.ResultData.Select(x => x.displaySymbol).FirstOrDefault(),
                    recordsFiltered = res.ResultData.Select(x => x.displaySymbol).FirstOrDefault(),
                });
            }
        }

        [HttpPost]
        public IActionResult UserListFilter()
        {
            FilterData data = new FilterData();
            data.sort = "symbol asc";
            int start = Convert.ToInt32(Request.Form["start"]);
            int length = Convert.ToInt32(Request.Form["length"]);
            if (length == 5)
            {
                start = start / 5;
            }
            else if (length == 10)
            {
                start = start / 10;
            }
            if (length == 25)
            {
                start = start / 25;
            }
            if (length == 50)
            {
                start = start / 50;
            }
            if (length == 100)
            {
                start = start / 100;
            }
            string searchValue = Request.Form["search[value]"];
            string sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"] + "][data]"];
            string sortDirection = Request.Form["order[0][dir]"];
            data.sort = sortColumn + " " + sortDirection;
            data.start = start.ToString();
            data.length = length.ToString();
            ResultObject<List<Usersdetails>> res = new ResultObject<List<Usersdetails>>();

            if (searchValue == "")
            {
                res = _IAdminMgr.getUser("viewuser", "", "", data);
            }
            else if (searchValue != "")
            {
                res = _IAdminMgr.getUser("userserach", searchValue, "", data);
            }

            if (searchValue != "")
            {
                return Json(new
                {
                    data = res.ResultData.Skip(start).Take(length),
                    draw = Request.Form["draw"],
                    recordsTotal = res.ResultData.Select(x => x.userCount).FirstOrDefault(),
                    recordsFiltered = res.ResultData.Select(x => x.userCount).FirstOrDefault(),
                });
            }
            else
            {
                return Json(new
                {
                    data = res.ResultData,
                    draw = Request.Form["draw"],
                    recordsTotal = res.ResultData.Select(x => x.userCount).FirstOrDefault(),
                    recordsFiltered = res.ResultData.Select(x => x.userCount).FirstOrDefault(),
                });
            }
        }


        [HttpPost]
        public IActionResult AllOrderListFilter()
        {
            FilterData data = new FilterData();
            data.sort = "id desc";
            int start = Convert.ToInt32(Request.Form["start"]);
            int length = Convert.ToInt32(Request.Form["length"]);
            if (length == 10)
            {
                start = start / 10;
            }
            if (length == 25)
            {
                start = start / 25;
            }
            if (length == 50)
            {
                start = start / 50;
            }
            if (length == 100)
            {
                start = start / 100;
            }
            string searchValue = Request.Form["search[value]"];
            string sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"] + "][data]"];
            string sortDirection = Request.Form["order[0][dir]"];

            if(sortColumn.Contains("name"))
            {
                sortColumn = "first_name";
            }

            data.sort = sortColumn + " " + sortDirection;
            data.start = start.ToString();
            data.length = length.ToString();
        
            ResultObject<List<OrderData>> res = new ResultObject<List<OrderData>>();
            try
            {
                if (searchValue == "")
                {
                    res = _IUserOrderMgr.viewOrderDetailFilter("viewAllOrderPagination", data);
                }
                else
                {
                    data.userType = searchValue;
                    res = _IUserOrderMgr.viewOrderDetailFilter("viewAllOrderPaginationFilter", data);

                }
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            if (searchValue != "")
            {
                return Json(new
                {
                    data = res.ResultData.Skip(start).Take(length),

                    draw = Request.Form["draw"],
                    recordsTotal = res.ResultData.Select(x => x.orderCount).FirstOrDefault(),
                    recordsFiltered = res.ResultData.Select(x => x.orderCount).FirstOrDefault(),
                });
            }
            else
            {
                return Json(new
                {
                    data = res.ResultData,
                    draw = Request.Form["draw"],
                    recordsTotal = res.ResultData.Select(x => x.orderCount).FirstOrDefault(),
                    recordsFiltered = res.ResultData.Select(x => x.orderCount).FirstOrDefault(),
                });
            }
        }
    }
}
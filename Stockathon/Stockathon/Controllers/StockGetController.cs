﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SA_BaseObjet;
using SA_BaseObjet.StocksApi;
using SA_Mgr_Interface.StocksApi;
using SA_Utilities;
using SA_Utilities.Enum;

namespace Stockathon.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class StockGetController : ControllerBase
    {
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IStocksApiMgr _IStocksApiMgr;

        public StockGetController(IStocksApiMgr IStocksApiMgr, IConfiguration iConfig, IHttpContextAccessor httpContextAccessor)
        {
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IStocksApiMgr = IStocksApiMgr;
        }

        [HttpGet]
        public dynamic market_news()
        {
            ResultObject<List<dynamic>> res = new ResultObject<List<dynamic>>();
            try
            {
                res = _IStocksApiMgr.getData("getMarketNews", "", "");
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "No Record Found"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getMarketNews", "StockGet", "market_news", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }

        [HttpGet]
        public dynamic seoList()
        {
            ResultObject<List<dynamic>> res = new ResultObject<List<dynamic>>();
            try
            {
                res = _IStocksApiMgr.getData("getSeoList", "", "");
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "No Record Found"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("planList", "StockGet", "planList", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }

        [HttpGet]
        public dynamic planList()
        {
            ResultObject<List<dynamic>> res = new ResultObject<List<dynamic>>();
            try
            {
                res = _IStocksApiMgr.getData("getPlanList", "", "");
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "No Record Found"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("planList", "StockGet", "planList", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }


        [HttpGet]
        public dynamic tickerList()
        {
            ResultObject<List<dynamic>> res = new ResultObject<List<dynamic>>();
            try
            {
                res = _IStocksApiMgr.getData("getTickerData", "", "");
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "No Record Found"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("getTickerData", "StockGet", "getTickerData", 0, ex.Message.ToString());
                return new
                {
                    result = 0,
                    message = "Invalid Data"
                } as dynamic;
            }
        }
    }
}
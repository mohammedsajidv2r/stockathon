﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using SA_BaseObjet;
using SA_BaseObjet.Admin;
using SA_Mgr_Interface.Admin;
using SA_Utilities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stockathon.Services
{
    public class SAUser
    {
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IUserOrderMgr _IUserOrderMgr;

        public SAUser(IConfiguration iConfig, IHttpContextAccessor httpContextAccessor, IUserOrderMgr IUserOrderMgr)
        {
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IUserOrderMgr = IUserOrderMgr;
        }
        public ResultObject<List<PlanOrderData>> viewUserPlan(string req,string Email)
        {
            ResultObject<List<PlanOrderData>> res = new ResultObject<List<PlanOrderData>>();
            try
            {
                res = _IUserOrderMgr.viewPlanDetail(req, Email);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> userPlanUpdate(List<PlanOrderData> model)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                if (model[0].PlanFlag == "1")
                {
                    List<PlanOrderData> res1 = model.Where(r => r.selectPlan != null).ToList();
                    String str = "[{\"plan_id\":\"" + res1[0].plan_features_id + "\",\"plan_name\":\"" + res1[0].plan_name + "\",\"plan_duration\":\"" + res1[0].plan_duration + "\" }]";
                    res1[0].order_plans_json = str;
                    string sdate = "";
                    string edate = "";
                    if (res1[0].plan_duration == "1")
                    {
                        sdate = DateTime.Now.ToString("yyyy-MM-dd");
                        edate = DateTime.Now.AddMonths(1).ToString("yyyy-MM-dd");
                    }
                    else if (res1[0].plan_duration == "3")
                    {
                        sdate = DateTime.Now.ToString("yyyy-MM-dd");
                        edate = DateTime.Now.AddMonths(3).ToString("yyyy-MM-dd");
                    }
                    else if (res1[0].plan_duration == "6")
                    {
                        sdate = DateTime.Now.ToString("yyyy-MM-dd");
                        edate = DateTime.Now.AddMonths(6).ToString("yyyy-MM-dd");
                    }
                    else if (res1[0].plan_duration == "12")
                    {
                        sdate = DateTime.Now.ToString("yyyy-MM-dd");
                        edate = DateTime.Now.AddMonths(12).ToString("yyyy-MM-dd");
                    }
                    res1[0].start_date = sdate;
                    res1[0].end_date = edate;
                    res1[0].membership_id = "0";
                    //res1[0].plan_id_list = '{' + res1[0].plan_features_id + '}';
                    res1[0].plan_id_list = res1[0].plan_features_id;
                    res = _IUserOrderMgr.userPlanUpdate("AddUpdateUserPlan", res1);
                }
                else if (model[0].PlanFlag == "0")
                {
                    res = _IUserOrderMgr.userPlanUpdate("AddUpdateUserPlan", model);
                }
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<PlanData>> viewPlan(string req, string param)
        {
            ResultObject<List<PlanData>> res = new ResultObject<List<PlanData>>();
            try
            {
                res = _IUserOrderMgr.viewPlan(req, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> updatedetails(string req, string Id, string xml, string param, string sp)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IUserOrderMgr.updatedetails(req, Id, xml, param, sp);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
    }
}

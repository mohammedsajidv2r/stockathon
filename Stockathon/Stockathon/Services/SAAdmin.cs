﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using SA_BaseObjet;
using SA_BaseObjet.Admin;
using SA_BaseObjet.StocksApi;
using SA_Mgr_Interface.Admin;
using SA_Utilities;
using SA_Utilities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stockathon.Services
{
    public class SAAdmin
    {
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IAdminMgr _IAdminMgr;

        public SAAdmin(IConfiguration iConfig, IHttpContextAccessor httpContextAccessor, IAdminMgr IAdminMgr)
        {
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IAdminMgr = IAdminMgr;
        }

        public ResultObject<Tbl_Admin_User> adminLoginAuth(string RequestType, Tbl_Admin_User data)
        {
            ResultObject<Tbl_Admin_User> res = new ResultObject<Tbl_Admin_User>();
            try
            {
                res = _IAdminMgr.adminLoginAuth(RequestType, data);
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("SAAdmin", "adminLoginAuth", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<Tbl_Admin_User> checkLogin()
        {
            ResultObject<Tbl_Admin_User> loginChk = new ResultObject<Tbl_Admin_User>();
            try
            {
                loginChk.ResultData = new Tbl_Admin_User();
                loginChk.ResultData = _httpContextAccessor.HttpContext.Session.GetObject<Tbl_Admin_User>("AuthLoginData");
                loginChk.Result = ResultType.Success;
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("SAAdmin", "checkLogin", "checkLogin", 0, ex.Message.ToString());
                _httpContextAccessor.HttpContext.Session.Clear();
            }
            if (loginChk.ResultData == null)
            {
                loginChk.Result = ResultType.Error;
            }
            return loginChk;
        }

        public void logOut()
        {
            _httpContextAccessor.HttpContext.Session.Clear();
        }

        public ResultObject<string> stockcron(string reqtype, string stock, string stocksymbolid, string sp)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.submitdetails(reqtype, stocksymbolid, stock, "", sp);
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("SAAdmin", "stockcron", reqtype, 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        #region userprofile
        public ResultObject<string> updateProfile(string RequestType, string param1, Tbl_Admin_User data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updateProfile(RequestType, param1, data);
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("SAAdmin", "updateProfile", RequestType, 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> updatePassword(string RequestType, string param1, string param2, string param3)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updatePassword(RequestType, param1, param2, param3);
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("SAAdmin", "updatePassword", RequestType, 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<string> resetpassword(string RequestType, string email)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updatePassword(RequestType, email, "", "");
                if (res.Result == ResultType.Success)
                {
                    //email = "mohammedsajid.v2r@gmail.com";
                    string idData = res.ResultData.ToString() + "/" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    string encrypPass = Encryption.EncryptString(idData, Encryption.passkey);
                    encrypPass = encrypPass.Replace('+', '_');
                    encrypPass = encrypPass.Replace('/', '~');
                    string url = "";
                    try
                    {
                        url = _httpContextAccessor.HttpContext.Request.Scheme + "://" + _httpContextAccessor.HttpContext.Request.Host.Value;
                    }
                    catch (Exception ex)
                    {
                        url = "https://services.stockathon.com/";
                    }

                    string body = "Hello admin! <br /><br />" +
                      "You are receiving this email because we recevied a password reset request for your account.<br /><br />" +
                      "You are just one step away from reset your password. <br />" +
                       url + "/resetpassword/" + encrypPass + "<br /><br />" +
                       "Thank you <br />" +
                       "Team Stockathon.<br />" +
                       url + " <br /><br />" +
                      "Copyright © 2020 Stockathon., All rights reserved.";

                    string subject = "Reset Password Request";
                    SentEmail.emailSent(body, subject, SentEmail.fromemail, email);
                    res.ResultMessage = "You will receive a password reset link at your e-mail address. Please click on that link to reset your password";
                }
                else
                {
                    res.Result = ResultType.Info;
                    res.ResultMessage = "User with this e-mail address is not registered with Stockathon";
                }
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
                ErrorLogs._InsertLogs("SAAdmin", "resetpassword", RequestType, 0, ex.Message.ToString());
            }
            return res;
        }

        public string ForgotPass(string userID)
        {
            string uid = "error";
            try
            {
                string encryp = userID;
                userID = userID.Replace('_', '+');
                userID = userID.Replace('~', '/');
                uid = Encryption.DecryptString(userID, Encryption.passkey);
                string[] idData = uid.Split("/");
                DateTime genDate = DateTime.Parse(idData[1]);
                genDate = genDate.AddMinutes(60);
                if (genDate >= DateTime.Now)
                {
                    uid = "success";
                    ResultObject<string> res = new ResultObject<string>();
                    res = _IAdminMgr.updatePassword("resetpasswordlinkcheck", "", encryp, idData[0]);
                    if (res.ResultData == "1")
                    {
                        uid = "success";
                    }
                    else
                    {
                        uid = "error";
                    }
                }
                else
                {
                    uid = "error";
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("SAAdmin", "ForgotPass", userID, 0, ex.Message.ToString());
                uid = "error";
            }
            return uid;
        }

        public string emailVerify(string userID)
        {
            string uid = "error";
            try
            {
                string encryp = userID;
                userID = userID.Replace('_', '+');
                userID = userID.Replace('~', '/');
                uid = Encryption.DecryptString(userID, Encryption.passkey);
                string[] idData = uid.Split("/");
                DateTime genDate = DateTime.Parse(idData[1]);
                genDate = genDate.AddMinutes(60);
                if (genDate >= DateTime.Now)
                {
                    uid = "success";
                    ResultObject<string> res = new ResultObject<string>();
                    res = _IAdminMgr.updatePassword("emailVerify", "", encryp, idData[0]);
                    if (res.ResultData == "1")
                    {
                        uid = "success";
                    }
                    else if (res.ResultData == "2")
                    {
                        uid = "This Email Id Already Active";
                    }
                    else
                    {
                        uid = "error";
                    }
                }
                else
                {
                    uid = "error";
                }
            }
            catch (Exception ex)
            {
                uid = "error";
                ErrorLogs._InsertLogs("SAAdmin", "emailVerify", userID, 0, ex.Message.ToString());
            }
            return uid;
        }

        public ResultObject<string> updateresetpass(string reqtype, string param1, string param2, string param3)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                string encryp = param3;
                param3 = param3.Replace('_', '+');
                param3 = param3.Replace('~', '/');
                string uid = Encryption.DecryptString(param3, Encryption.passkey);
                string[] idData = uid.Split("/");
                param3 = idData[0];
                res = _IAdminMgr.updatePassword(reqtype, param1, encryp, param3);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
                ErrorLogs._InsertLogs("SAAdmin", "updateresetpass", reqtype, 0, ex.Message.ToString());
            }
            return res;
        }

        #endregion
        public ResultObject<Tbl_Admin_Intrinsic_Values> adminvVal(string RequestType)
        {
            ResultObject<Tbl_Admin_Intrinsic_Values> res = new ResultObject<Tbl_Admin_Intrinsic_Values>();
            try
            {
                res = _IAdminMgr.adminvVal(RequestType);
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("SAAdmin", "adminvVal", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        public ResultObject<string> update(string reqtype, string stock, string stocksymbolid, string xml, string sp)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.submitdetails(reqtype, stocksymbolid, stock, xml, sp);
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs("SAAdmin", "stockcron", reqtype, 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        #region cms
        public ResultObject<List<ConfigurationData>> viewConfiguration()
        {
            ResultObject<List<ConfigurationData>> res = new ResultObject<List<ConfigurationData>>();
            try
            {
                res = _IAdminMgr.viewConfiguration("viewconfiguation", "", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<string> editConfiguration(ConfigurationData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editConfiguration("updateconfiguration", "", data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }



        public ResultObject<List<FaqData>> viewfaq(string req, string param)
        {
            ResultObject<List<FaqData>> res = new ResultObject<List<FaqData>>();
            try
            {
                res = _IAdminMgr.viewfaq(req, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editfaq(string reqtype, string param, FaqData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editfaq(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<CmsPageData>> cmsMaster()
        {
            ResultObject<List<CmsPageData>> res = new ResultObject<List<CmsPageData>>();
            try
            {
                res = _IAdminMgr.cmsMaster("viewcms", "", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<CmsPageData>> cmsMasterreq(string reqtype, string param)
        {
            ResultObject<List<CmsPageData>> res = new ResultObject<List<CmsPageData>>();
            try
            {
                res = _IAdminMgr.cmsMaster(reqtype, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editCmsMaster(string reqtype, string param, CmsPageData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editCmsMaster(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion

        public ResultObject<List<tbl_PageSEO>> viewSeo(string reqtype, string id)
        {
            ResultObject<List<tbl_PageSEO>> res = new ResultObject<List<tbl_PageSEO>>();
            try
            {
                res = _IAdminMgr.viewSeo(reqtype, id, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<StockData>> stockWithId(string reqtype, string symbol)
        {
            ResultObject<List<StockData>> res = new ResultObject<List<StockData>>();
            try
            {
                FilterData data = new FilterData();
                res = _IAdminMgr.getStocksDetals(reqtype, symbol, "", data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<List<Tbl_Seo_Stock>> viewSeoStock(string reqtype, string id)
        {
            ResultObject<List<Tbl_Seo_Stock>> res = new ResultObject<List<Tbl_Seo_Stock>>();
            try
            {
                res = _IAdminMgr.viewSeoStock(reqtype, id, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }




    }
}

#pragma checksum "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9b143853e309c8627b1bafc5e5ddca5210673622"
// <auto-generated/>
#pragma warning disable 1591
namespace Stockathon.Pages.CustomTools
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Utilities.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_BaseObjet;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_DA_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_DA;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Mgr;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Mgr_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Mgr_Interface.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_BaseObjet.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
using System.ComponentModel.DataAnnotations;

#line default
#line hidden
#nullable disable
    public partial class PlanPopupModel : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "modal fade bd-example-modal-lg");
            __builder.AddAttribute(2, "tabindex", "-1");
            __builder.AddAttribute(3, "role", "dialog");
            __builder.AddAttribute(4, "id", "large-Modal");
            __builder.AddAttribute(5, "aria-labelledby", "myLargeModalLabel");
            __builder.AddAttribute(6, "aria-hidden", "true");
            __builder.AddMarkupContent(7, "\r\n    ");
            __builder.OpenElement(8, "div");
            __builder.AddAttribute(9, "class", "modal-dialog modal-lg");
            __builder.AddAttribute(10, "role", "document");
            __builder.AddMarkupContent(11, "\r\n        ");
            __builder.OpenElement(12, "div");
            __builder.AddAttribute(13, "class", "modal-content");
            __builder.AddMarkupContent(14, "\r\n            ");
            __builder.OpenElement(15, "div");
            __builder.AddAttribute(16, "class", "modal-header");
            __builder.AddMarkupContent(17, "\r\n");
#nullable restore
#line 7 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                  
                    if (PData.plan_features_id > 0)
                    {

#line default
#line hidden
#nullable disable
            __builder.AddContent(18, "                        ");
            __builder.AddMarkupContent(19, "<h5 class=\"modal-title\" id=\"myLargeModalLabel\">Edit Plan</h5>\r\n");
#nullable restore
#line 11 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                    }
                    else
                    {

#line default
#line hidden
#nullable disable
            __builder.AddContent(20, "                        ");
            __builder.AddMarkupContent(21, "<h5 class=\"modal-title\" id=\"myLargeModalLabel\">Create Plan</h5>\r\n");
#nullable restore
#line 15 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                    }
                

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(22, "\r\n                ");
            __builder.OpenElement(23, "button");
            __builder.AddAttribute(24, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 18 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                   OnCancel

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(25, "type", "button");
            __builder.AddAttribute(26, "class", "close");
            __builder.AddAttribute(27, "aria-label", "Close");
            __builder.AddMarkupContent(28, "\r\n                    ");
            __builder.AddMarkupContent(29, @"<svg aria-hidden=""true"" xmlns=""http://www.w3.org/2000/svg"" width=""24"" height=""24"" viewBox=""0 0 24 24"" fill=""none"" stroke=""currentColor"" stroke-width=""2"" stroke-linecap=""round"" stroke-linejoin=""round"" class=""feather feather-x"">
                        <line x1=""18"" y1=""6"" x2=""6"" y2=""18""></line>
                        <line x1=""6"" y1=""6"" x2=""18"" y2=""18""></line>
                    </svg>
                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(30, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(31, "\r\n            ");
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(32);
            __builder.AddAttribute(33, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 27 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                              PData

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(34, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 27 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                     OnConfirm

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(35, "OnInvalidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 27 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                                                  HandleInvalidSubmit

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(36, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.AddMarkupContent(37, "\r\n                ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(38);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(39, "\r\n                ");
                __builder2.OpenElement(40, "div");
                __builder2.AddAttribute(41, "class", "modal-body");
                __builder2.AddMarkupContent(42, "\r\n                    ");
                __builder2.OpenElement(43, "div");
                __builder2.AddAttribute(44, "class", "row");
                __builder2.AddMarkupContent(45, "\r\n");
#nullable restore
#line 31 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                          
                            if (PData.plan_features_id > 0)
                            {

#line default
#line hidden
#nullable disable
                __builder2.AddContent(46, "                                ");
                __builder2.OpenElement(47, "div");
                __builder2.AddAttribute(48, "class", "form-group mb-4 col-lg-6");
                __builder2.AddMarkupContent(49, "\r\n                                    ");
                __builder2.OpenElement(50, "input");
                __builder2.AddAttribute(51, "type", "hidden");
                __builder2.AddAttribute(52, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 35 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                                PData.plan_features_id

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(53, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => PData.plan_features_id = __value, PData.plan_features_id));
                __builder2.SetUpdatesAttributeName("value");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(54, "\r\n                                    ");
                __builder2.AddMarkupContent(55, "<label for=\"name\">Plan Old Price</label>\r\n                                    ");
                __builder2.OpenElement(56, "input");
                __builder2.AddAttribute(57, "type", "text");
                __builder2.AddAttribute(58, "class", "form-control");
                __builder2.AddAttribute(59, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 37 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              PData.plan_old_price

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(60, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => PData.plan_old_price = __value, PData.plan_old_price));
                __builder2.SetUpdatesAttributeName("value");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(61, "\r\n                                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(62, "\r\n                                ");
                __Blazor.Stockathon.Pages.CustomTools.PlanPopupModel.TypeInference.CreateValidationMessage_0(__builder2, 63, 64, 
#nullable restore
#line 39 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                          () => PData.plan_old_price

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(65, "\r\n                                ");
                __builder2.OpenElement(66, "div");
                __builder2.AddAttribute(67, "class", "form-group mb-4 col-lg-6");
                __builder2.AddMarkupContent(68, "\r\n                                    ");
                __builder2.AddMarkupContent(69, "<label for=\"feature\">Plan Discounted Price</label>\r\n                                    ");
                __builder2.OpenElement(70, "input");
                __builder2.AddAttribute(71, "type", "text");
                __builder2.AddAttribute(72, "class", "form-control");
                __builder2.AddAttribute(73, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 42 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              PData.plan_pricing

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(74, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => PData.plan_pricing = __value, PData.plan_pricing));
                __builder2.SetUpdatesAttributeName("value");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(75, "\r\n                                    ");
                __Blazor.Stockathon.Pages.CustomTools.PlanPopupModel.TypeInference.CreateValidationMessage_1(__builder2, 76, 77, 
#nullable restore
#line 43 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              () => PData.plan_pricing

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(78, "\r\n                                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(79, "\r\n                                ");
                __builder2.OpenElement(80, "div");
                __builder2.AddAttribute(81, "class", "form-group mb-4 col-lg-6");
                __builder2.AddMarkupContent(82, "\r\n                                    ");
                __builder2.AddMarkupContent(83, "<label for=\"fcode\">Short Message</label>\r\n                                    ");
                __builder2.OpenElement(84, "input");
                __builder2.AddAttribute(85, "type", "text");
                __builder2.AddAttribute(86, "class", "form-control");
                __builder2.AddAttribute(87, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 47 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              PData.plan_short_message

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(88, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => PData.plan_short_message = __value, PData.plan_short_message));
                __builder2.SetUpdatesAttributeName("value");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(89, "\r\n\r\n                                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(90, "\r\n");
#nullable restore
#line 50 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                            }
                            else
                            {

#line default
#line hidden
#nullable disable
                __builder2.AddContent(91, "                                ");
                __builder2.OpenElement(92, "div");
                __builder2.AddAttribute(93, "class", "form-group mb-4 col-lg-6");
                __builder2.AddMarkupContent(94, "\r\n                                    ");
                __builder2.AddMarkupContent(95, "<label for=\"name\">Plan Name</label>\r\n                                    ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(96);
                __builder2.AddAttribute(97, "Id", "plan_name");
                __builder2.AddAttribute(98, "Class", "form-control");
                __builder2.AddAttribute(99, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 55 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                                                                 PData.plan_name

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(100, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => PData.plan_name = __value, PData.plan_name))));
                __builder2.AddAttribute(101, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => PData.plan_name));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(102, "\r\n                                    ");
                __Blazor.Stockathon.Pages.CustomTools.PlanPopupModel.TypeInference.CreateValidationMessage_2(__builder2, 103, 104, 
#nullable restore
#line 56 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              () => PData.plan_name

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(105, "\r\n                                    ");
                __builder2.OpenElement(106, "input");
                __builder2.AddAttribute(107, "type", "hidden");
                __builder2.AddAttribute(108, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 57 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                                PData.plan_features_id

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(109, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => PData.plan_features_id = __value, PData.plan_features_id));
                __builder2.SetUpdatesAttributeName("value");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(110, "\r\n                                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(111, "\r\n");
                __builder2.AddContent(112, "                                ");
                __builder2.OpenElement(113, "div");
                __builder2.AddAttribute(114, "class", "form-group mb-4 col-lg-6");
                __builder2.AddMarkupContent(115, "\r\n                                    ");
                __builder2.AddMarkupContent(116, "<label for=\"duration\">Plan Duration</label>\r\n                                    ");
                __builder2.OpenElement(117, "input");
                __builder2.AddAttribute(118, "type", "text");
                __builder2.AddAttribute(119, "class", "form-control");
                __builder2.AddAttribute(120, "placeholder", "Duration");
                __builder2.AddAttribute(121, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 70 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              PData.plan_duration

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(122, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => PData.plan_duration = __value, PData.plan_duration));
                __builder2.SetUpdatesAttributeName("value");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(123, "\r\n                                    ");
                __Blazor.Stockathon.Pages.CustomTools.PlanPopupModel.TypeInference.CreateValidationMessage_3(__builder2, 124, 125, 
#nullable restore
#line 71 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              () => PData.plan_duration

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(126, "\r\n                                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(127, "\r\n");
                __builder2.AddContent(128, "                                ");
                __builder2.OpenElement(129, "div");
                __builder2.AddAttribute(130, "class", "form-group mb-4 col-lg-6");
                __builder2.AddMarkupContent(131, "\r\n                                    ");
                __builder2.AddMarkupContent(132, "<label for=\"price\">Discount Price</label>\r\n                                    ");
                __builder2.OpenElement(133, "input");
                __builder2.AddAttribute(134, "type", "text");
                __builder2.AddAttribute(135, "class", "form-control");
                __builder2.AddAttribute(136, "placeholder", "Price");
                __builder2.AddAttribute(137, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 80 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              PData.plan_pricing

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(138, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => PData.plan_pricing = __value, PData.plan_pricing));
                __builder2.SetUpdatesAttributeName("value");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(139, "\r\n                                    ");
                __Blazor.Stockathon.Pages.CustomTools.PlanPopupModel.TypeInference.CreateValidationMessage_4(__builder2, 140, 141, 
#nullable restore
#line 81 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              () => PData.plan_pricing

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(142, "\r\n\r\n                                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(143, "\r\n                                ");
                __builder2.OpenElement(144, "div");
                __builder2.AddAttribute(145, "class", "form-group mb-4 col-lg-6");
                __builder2.AddMarkupContent(146, "\r\n                                    ");
                __builder2.AddMarkupContent(147, "<label for=\"price\">Price</label>\r\n                                    ");
                __builder2.OpenElement(148, "input");
                __builder2.AddAttribute(149, "type", "text");
                __builder2.AddAttribute(150, "class", "form-control");
                __builder2.AddAttribute(151, "placeholder", "Price");
                __builder2.AddAttribute(152, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 86 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              PData.plan_old_price

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(153, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => PData.plan_old_price = __value, PData.plan_old_price));
                __builder2.SetUpdatesAttributeName("value");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(154, "\r\n                                    ");
                __Blazor.Stockathon.Pages.CustomTools.PlanPopupModel.TypeInference.CreateValidationMessage_5(__builder2, 155, 156, 
#nullable restore
#line 87 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              () => PData.plan_old_price

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(157, "\r\n\r\n                                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(158, "\r\n");
                __builder2.AddContent(159, "                                ");
                __builder2.OpenElement(160, "div");
                __builder2.AddAttribute(161, "class", "form-group mb-4 col-lg-6");
                __builder2.AddMarkupContent(162, "\r\n                                    ");
                __builder2.AddMarkupContent(163, "<label for=\"message\">Plan Short Message</label>\r\n                                    ");
                __builder2.OpenElement(164, "input");
                __builder2.AddAttribute(165, "type", "text");
                __builder2.AddAttribute(166, "class", "form-control");
                __builder2.AddAttribute(167, "placeholder", "Message");
                __builder2.AddAttribute(168, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 96 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              PData.plan_short_message

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(169, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => PData.plan_short_message = __value, PData.plan_short_message));
                __builder2.SetUpdatesAttributeName("value");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(170, "\r\n                                    ");
                __Blazor.Stockathon.Pages.CustomTools.PlanPopupModel.TypeInference.CreateValidationMessage_6(__builder2, 171, 172, 
#nullable restore
#line 97 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                              () => PData.plan_short_message

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(173, "\r\n\r\n                                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(174, "\r\n");
#nullable restore
#line 106 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                    
                            }
                        

#line default
#line hidden
#nullable disable
                __builder2.AddMarkupContent(175, "\r\n                    ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(176, "\r\n                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(177, "\r\n                ");
                __builder2.OpenElement(178, "div");
                __builder2.AddAttribute(179, "class", "modal-footer");
                __builder2.AddMarkupContent(180, "\r\n                    ");
                __builder2.AddMarkupContent(181, "<button type=\"submit\" class=\"btn btn-primary\">Submit</button>\r\n                    ");
                __builder2.OpenElement(182, "button");
                __builder2.AddAttribute(183, "class", "btn");
                __builder2.AddAttribute(184, "type", "button");
                __builder2.AddAttribute(185, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 114 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
                                                                 OnCancel

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddMarkupContent(186, "<i class=\"flaticon-cancel-12\"></i> Close");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(187, "\r\n                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(188, "\r\n\r\n            ");
            }
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(189, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(190, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(191, "\r\n");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 123 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\CustomTools\PlanPopupModel.razor"
       
    [Parameter] public long DataID { get; set; }
    [Parameter] public EventCallback OnCancel { get; set; }
    [Parameter] public EventCallback OnConfirm { get; set; }
    [Parameter] public PlanData PData { get; set; }

    private string StatusMessage;
    private string StatusClass;

    protected override void OnInitialized()
    {
    }

    protected override void OnAfterRender(bool firstRender)
    {
        JSRuntime.InvokeVoidAsync("CSShowModel", "large-Modal");
    }

    protected void HandleValidSubmit()
    {
        StatusClass = "alert-info";
        StatusMessage = DateTime.Now + " Handle valid submit";
    }
    protected void HandleInvalidSubmit()
    {
        StatusClass = "alert-danger";
        StatusMessage = DateTime.Now + " Handle invalid submit";
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpContextAccessor httpContextAccessor { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration iConfig { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
namespace __Blazor.Stockathon.Pages.CustomTools.PlanPopupModel
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateValidationMessage_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_2<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_3<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_4<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_5<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_6<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591

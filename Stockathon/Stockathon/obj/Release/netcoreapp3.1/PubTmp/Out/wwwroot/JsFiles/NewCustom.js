﻿function CSLoaderFadeIn() {
    $('.theme-loader').fadeIn('slow', function () {
    });
}

function CSLoaderFadeOut() {
    $('.theme-loader').fadeOut('slow', function () {
    });
}


function CSShowModel(ModelID) {
    $("#" + ModelID).modal('show');
}

function CSHideModel(ModelID) {
    $("#" + ModelID).modal('hide');
}

function CSAlertMsg(type, msg) {
    Snackbar.show({
        text: msg,
        pos: 'top-center'
    });
}

function CSReloadFunc() {
    location.reload();
}

function CSWindowReload() {
    window.location.reload();
}

window.setTitle = (title) => {
    document.title = title;
}

function changespantext(id, value) {
    $('#' + id).html(value);
}



function isNumberKey() {
    $("#digit").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#errormsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
}

function isDecimalKey() {
    $(document).on('keydown', 'input[pattern]', function (e) {
        var input = $(this);
        var oldVal = input.val();
        var regex = new RegExp(input.attr('pattern'), 'g');

        setTimeout(function () {
            var newVal = input.val();
            if (!regex.test(newVal)) {
                input.val(oldVal);
            }
        }, 0);
    });
}

function CSBindDataTableRemove(tableId) {
    $('#' + tableId).DataTable().destroy();
}

//datatable  Stocklist

function CSStocksDTBind() {
    $.fn.dataTable.ext.errMode = 'none';

    reviewtable = $("#termTable").DataTable({
        "ajax": {
            "url": "api/Admin/SymbolListFilter",
            "type": "POST",
            "datatype": "json",
            "data": function (d) {
            }
        },
        "columns": [
            { "data": "symbol" },
            { "data": "description" },
            //{ "data": "type" },
            {
                "data": "type",
                "render": function (data, type, full, meta) {
                    if (data == 'Not Available') {
                        return "<button type='button' style='font-size: 10px;' class='btn btn-primary' onclick='CallPopupOrderReview(" + '"' + full["symbol"] + '"' + "," + '"' + full["stock_symbol_id"] + '"' + ");' >Not Available</button>";
                    }
                    else if (data == 'In-Process') {
                        return "<span>In-Process</span>"
                    }
                    else {
                        return "<span>Available</span>"
                    }


                }
            }//,
            //{
            //    "data": "symbol",
            //    "render": function (data, type, full, meta) {
            //        if (data != null) {
            //            return "<ul class='table-controls'>" +
            //                "<li><a href='javascript:CallPopupStockProfile(" + '"' + full["symbol"] + '"' + "," + '"' + full["stock_symbol_id"] + '"' + ");' data-placement='top'  class= 'bs-tooltip' id='editor_edit1'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'    class='feather feather-edit-2 p-1 br-6 mb-1'> <path d='M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z'></path></svg></a></li></ul>";
            //        }
            //        else {
            //            return "<span></span>";
            //        }
            //
            //
            //    }
            //}
        ],
        "serverSide": "true",
        "order": [0, "asc"],
        "processing": "true",
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        //"aoColumns": [
        //    null,
        //    null,
        //    null, // <-- disable sorting for column 3
        //    null
        //],
        //'columnDefs': [{
        //    "targets": [2],
        //    "orderable": false
        //}],
        "aoColumnDefs": [
            {
                bSortable: false,
                aTargets: [-1]
            }
        ],
    })
}

function CallPopupOrderReview(data, stock_symbol_id) {
    $("#mailData").text(data);
    $("#Id").text(stock_symbol_id);
    $("#HidUserBtn").click();
}

function CallPopupStockProfile(data, stock_symbol_id) {
    $("#mailData").text(data);
    $("#Id").text(stock_symbol_id);
    $("#HidStockBtn").click();
}

function getmailDataOrder() {
    return $("#mailData").text();
}


function getmailDataReview() {
    return $("#Id").text();
}

//stocklist end


//datatable user  //userlist
function CSUserDTBind() {
    $.fn.dataTable.ext.errMode = 'none';

    reviewtable = $("#userTable").DataTable({
        "ajax": {
            "url": "api/Admin/UserListFilter",
            "type": "POST",
            "datatype": "json",
            "data": function (d) {
            }
        },
        "columns": [
            { "data": "first_name" },
            { "data": "mobile" },
            { "data": "email" },
            {
                "data": "created_at",
                "render": function (data, type, full, meta) {
                    return "<span>" + data.split("T")[0] + "</span>"
                }
            },
            {
                "data": "social_flag",
                "render": function (data, type, full, meta) {
                    if (data == 2) {
                        return "<span>Facebook</span>"
                    }
                    else {
                        return "<span>Google</span>"
                    }
                }
            },
            {
                "data": "last_login",
                "render": function (data, type, full, meta) {
                    return "<span>" + data.split("T")[0] + "</span>" + " " + "<span>" + data.split("T")[1] + "</span>"
                }
            },
            { "data": "prime_status" },
            {
                "data": "user_id",
                "render": function (data, type, full, meta) {
                    if (data != null) {
                        return "<ul class='table-controls'><li><a href='javascript:CallPopupUser(" + '"' + data + '"' + ");' class= 'bs-tooltip' id='editor_edit'    ><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit-2 p-1 br-6 mb-1'> <path d='M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z'></path> </svg></a></li>";
                    }
                    else {
                        return "<span></span>";
                    }
                }

            }
        ],

        "serverSide": "true",
        "order": [0, "asc"],
        "processing": "true",
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "aoColumnDefs": [
            {
                bSortable: false,
                aTargets: [-1]
            }
        ],
    })
}

function CallPopupUser(data) {
    $("#userId").text(data);
    $("#HidUserBtn").click();
}

function getmailDataUser() {
    return $("#userId").text();
}

//user ilst end


//seolist 
function CSBindDataTableSeo(tableId, customBind, DeforderColNo, sorting) {
    $.fn.dataTable.ext.errMode = 'none';
    DtDynamicVal = $('#' + tableId).DataTable({
        "order": [DeforderColNo, "asc"],
        "processing": "true",
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        // columnDefs: [{ type: 'date', 'targets': [DeforderColNo] }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],

    });
    $.fn.dataTable.ext.errMode = 'none';
}

function CSBindDataTableSeoStock(tableId, customBind, DeforderColNo, sorting) {
    $.fn.dataTable.ext.errMode = 'none';
    DtDynamicVal = $('#' + tableId).DataTable({
        "order": [DeforderColNo, "asc"],
        "processing": "true",
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        // columnDefs: [{ type: 'date', 'targets': [DeforderColNo] }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],

    });
    $.fn.dataTable.ext.errMode = 'none';
}
//seo end

//plan
function CSBindDataTable(tableId, customBind, DeforderColNo) {
    debugger;
    DtDynamicVal = $('#' + tableId).DataTable({
        "order": [DeforderColNo, "desc"],
        "processing": "true",
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        // columnDefs: [{ type: 'date', 'targets': [DeforderColNo] }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],

    });
    $.fn.dataTable.ext.errMode = 'none';
}
//plan end


//orderlist 
function CSOrderDTBind() {
    $.fn.dataTable.ext.errMode = 'none';
    ordertable = $("#individual-col-search").DataTable({
        "ajax": {
            "url": "api/Admin/AllOrderListFilter",
            "type": "POST",
            "datatype": "json",
            "data": function (d) {
            }
        },
        "columns": [
            { "data": "name" },
            { "data": "email" },
            { "data": "order_date_time" },
            { "data": "order_total_with_gst" },
            {
                "data": "txn_payment_status",
                render: function (data) {
                    if (data == 1) {

                        return '<a href="javascript: void (0)"><span class="shadow-none badge badge-primary">Success</span></a>';
                    }
                    else {
                        return '<a href="javascript: void (0)"><span class="shadow-none badge badge-danger">Failed</span></a>';
                    }
                }
            },
            {
                "data": "order_id",
                "render": function (data, type, full, meta) {
                    if (data != null) {
                        return "<button type='button' class='btn btn-primary' onclick='CallPopupOrder(" + '"' + data.toString() + '"' + ");' >View</button>";
                    }
                    else {
                        return "<span></span>"
                    }
                }
            }
        ],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1]
        }],
        "serverSide": "true",
        "order": [2, "desc"],
        "processing": "true",
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        }
    })
}

function CallPopupOrder(data) {
    // 
    $("#mailData").text(data);
    $("#HidUserBtn").click();
}

function getmailDataOrder() {
    return $("#mailData").text();
}


//order list end


//ckeditor
function CSBindCmsCkeditor() {
    if (CKEDITOR.instances['contentval']) CKEDITOR.instances['contentval'].destroy();
    CKEDITOR.config.removeButtons =
        'Save,NewPage,Preview,Print,PasteFromWord,PasteText,Paste,Copy,Cut,Undo,Redo,Replace,Find,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,Templates,Checkbox,CopyFormatting,RemoveFormat,NumberedList,BulletedList,Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Styles';
    CKEDITOR.replace('contentval');
    CKEDITOR.add
}

function getEditor(id) {
    var desc = CKEDITOR.instances[id].getData();
    return desc
}


function AutoCompleteBinding(StockData) {
    var StockDataArray = [];
    StockData.forEach(function (item) {
        var sdata = {
            value: item,
            label: item
        }
        StockDataArray.push(sdata);
    });
    $('.autocomplete-ui').autocomplete({
        source: StockDataArray,
        select: function (event, ui) {
            document.getElementById('HideBtnForIndustry').click();
        },
    });
}

function CSBindProgreeOnChange(count) {
        var prgtype = "bg-warning";

    if (count <= 45) {
        prgtype = "bg-warning";
    }
    else if (count >= 46 && count <= 70) {
        prgtype = "bg-success";
    }
    else if (count >= 71) {
        prgtype = "bg-danger";
    }
    $("#Cs_prog_bar").removeClass("bg-warning");
    $("#Cs_prog_bar").removeClass("bg-success");
    $("#Cs_prog_bar").removeClass("bg-danger");
    $("#Cs_prog_bar").addClass(prgtype);
}

function CSBindProgreeOnChangeDesc(count) {
    var prgtype = "bg-warning";

    if (count <= 100) {
        prgtype = "bg-warning";
    }
    else if (count >= 101 && count <= 140) {
        prgtype = "bg-success";
    }
    else if (count >= 141) {
        prgtype = "bg-danger";
    }
    $("#Cs_prog_barDescription").removeClass("bg-warning");
    $("#Cs_prog_barDescription").removeClass("bg-success");
    $("#Cs_prog_barDescription").removeClass("bg-danger");
    $("#Cs_prog_barDescription").addClass(prgtype);
}


function ddl() {
    var ss = $(".basic").select2({
        tags: true,
    });
}

function CSBindingMultiSelect(id) {
    $("#" + id).selectpicker({
        showTick: true,
        iconBase: 'fa',
        tickIcon: 'fa-check',
        noneSelectedText: '---Select Stock---',
        noneResultsText: 'No Data matched {0}',
        size: '8'
    });
}

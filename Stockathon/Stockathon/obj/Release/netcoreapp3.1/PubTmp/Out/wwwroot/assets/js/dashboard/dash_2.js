function CSBindDashboard(wlData, USubData) {
    debugger;
    WLApp = [];
    WLWeb = [];
    WLDate = [];
    wlData.forEach(function (item) {
        WLApp.push(parseInt(item.appcount));
        WLWeb.push(parseInt(item.webcount));
        WLDate.push(item.created_date);
    });

    UsubNew = [];
    UsubRenewals = [];
    UsubDate = [];
    Usubtotal = [];
    USubData.forEach(function (item) {
        UsubNew.push(parseInt(item.num_new_subscribers));
        UsubRenewals.push(parseInt(item.num_renewals));
        UsubDate.push(item.mYear);
        Usubtotal.push(item.num_subscribers);
    });

    var sumWeb = arrSum(WLWeb);
    var sumApp = arrSum(WLApp);

    try {

        // Registered App Users
        var regUser = [30, 60, 38, 52, 36, 40, 28, 38, 60, 38, 52, 36, 40, 28, 38];
        var regAppUser = {
            chart: {
                id: '',
                type: 'area',
                height: 50,
                sparkline: {
                    enabled: true
                },
            },
            stroke: {
                curve: 'smooth',
                width: 2,
            },
            series: [{
                name: 'Users',
                data: regUser
            }],
            labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'],
            yaxis: {
                min: 0,
                max: Math.max(...regUser) + 5
            },
            colors: ['#1b55e2'],
            tooltip: {
                x: {
                    show: false,
                }
            },
            fill: {
                type: "gradient",
                gradient: {
                    type: "vertical",
                    shadeIntensity: 1,
                    inverseColors: !1,
                    opacityFrom: .40,
                    opacityTo: .05,
                    stops: [45, 100]
                }
            },
        }

        // Unregistered App Users
        var unRegUser = [10, 20, 20, 25, 10, 15, 10, 20, 25, 22, 25, 20, 25, 15, 10];
        var unregAppUser = {
            chart: {
                id: '',
                type: 'area',
                height: 50,
                margin: 10,
                sparkline: {
                    enabled: true
                },
            },
            stroke: {
                curve: 'smooth',
                width: 2,
            },
            series: [{
                name: 'Users',
                data: unRegUser
            }],
            labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'],
            yaxis: {
                min: 0,
                max: Math.max(...unRegUser) + 5
            },
            colors: ['#e7515a'],
            tooltip: {
                x: {
                    show: false,
                }
            },
            fill: {
                type: "gradient",
                gradient: {
                    type: "vertical",
                    shadeIntensity: 1,
                    inverseColors: !1,
                    opacityFrom: .40,
                    opacityTo: .05,
                    stops: [45, 100]
                }
            },
        }

        // User Subscriptions
        var userSubscribtions = {
            chart: {
                height: 350,
                type: 'bar',
                stacked: true,
                toolbar: {
                    show: false,
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        position: 'top',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            plotOptions: {
                bar: {
                    horizontal: false,
                },
            },
            series: [{
                name: 'Renewals',
                data: UsubRenewals
            }, {
                name: 'New Subscribers',
                data: UsubNew
            },
            {
                name: 'Total Subscriptions',
                data: Usubtotal
            },
            ],
            xaxis: {
                type: 'text',
                categories: UsubDate,
            },
            legend: {
                position: 'top',
            },
            fill: {
                opacity: 1
            },
        }

        /*
          =================================
              watchlist Created
          =================================
      */
        var watchlist = {
            chart: {
                fontFamily: 'Nunito, sans-serif',
                height: 365,
                type: 'area',
                zoom: {
                    enabled: false
                },
                dropShadow: {
                    enabled: true,
                    opacity: 0.3,
                    blur: 5,
                    left: -7,
                    top: 22
                },
                toolbar: {
                    show: false
                },
                events: {
                    mounted: function (ctx, config) {
                        const highest1 = ctx.getHighestValueInSeries(0);
                        const highest2 = ctx.getHighestValueInSeries(1);

                        ctx.addPointAnnotation({
                            x: new Date(ctx.w.globals.seriesX[0][ctx.w.globals.series[0].indexOf(highest1)]).getTime(),
                            y: highest1,
                            label: {
                                style: {
                                    cssClass: 'd-none'
                                }
                            },
                            customSVG: {
                                SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#1b55e2" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
                                cssClass: undefined,
                                offsetX: -8,
                                offsetY: 5
                            }
                        })

                        ctx.addPointAnnotation({
                            x: new Date(ctx.w.globals.seriesX[1][ctx.w.globals.series[1].indexOf(highest2)]).getTime(),
                            y: highest2,
                            label: {
                                style: {
                                    cssClass: 'd-none'
                                }
                            },
                            customSVG: {
                                SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#e7515a" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
                                cssClass: undefined,
                                offsetX: -8,
                                offsetY: 5
                            }
                        })
                    },
                }
            },
            colors: ['#1b55e2', '#e7515a'],
            dataLabels: {
                enabled: false
            },
            markers: {
                discrete: [{
                    seriesIndex: 0,
                    dataPointIndex: 7,
                    fillColor: '#000',
                    strokeColor: '#000',
                    size: 5
                }, {
                    seriesIndex: 2,
                    dataPointIndex: 11,
                    fillColor: '#000',
                    strokeColor: '#000',
                    size: 4
                }]
            },
            subtitle: {
                text: 'Total Created Wachlist',
                align: 'left',
                margin: 0,
                offsetX: -10,
                offsetY: 35,
                floating: false,
                style: {
                    fontSize: '14px',
                    color: '#888ea8'
                }
            },
            title: {
                text: (sumApp + sumWeb),
                align: 'left',
                margin: 0,
                offsetX: -10,
                offsetY: 0,
                floating: false,
                style: {
                    fontSize: '25px',
                    color: '#0e1726'
                },
            },
            stroke: {
                show: true,
                curve: 'smooth',
                width: 2,
                lineCap: 'square'
            },
            series: [{
                name: 'App',
                data: WLApp
            }, {
                name: 'Website',
                data: WLWeb
            }],
            labels: WLDate,
            xaxis: {
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false
                },
                crosshairs: {
                    show: true
                },
                labels: {
                    offsetX: 0,
                    offsetY: 5,
                    style: {
                        fontSize: '12px',
                        fontFamily: 'Nunito, sans-serif',
                        cssClass: 'apexcharts-xaxis-title',
                    },
                }
            },
            yaxis: {
                labels: {
                    formatter: function (value, index) {
                        return value;
                    },
                    offsetX: -22,
                    offsetY: 0,
                    style: {
                        fontSize: '12px',
                        fontFamily: 'Nunito, sans-serif',
                        cssClass: 'apexcharts-yaxis-title',
                    },
                }
            },
            grid: {
                borderColor: '#e0e6ed',
                strokeDashArray: 5,
                xaxis: {
                    lines: {
                        show: true
                    }
                },
                yaxis: {
                    lines: {
                        show: false,
                    }
                },
                padding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: -10
                },
            },
            legend: {
                position: 'top',
                horizontalAlign: 'center',
                offsetY: -50,
                fontSize: '16px',
                fontFamily: 'Nunito, sans-serif',
                markers: {
                    width: 10,
                    height: 10,
                    strokeWidth: 0,
                    strokeColor: '#fff',
                    fillColors: undefined,
                    radius: 12,
                    onClick: undefined,
                    offsetX: 0,
                    offsetY: 0
                },
                itemMargin: {
                    horizontal: 0,
                    vertical: 20
                }
            },
            tooltip: {
                theme: 'dark',
                marker: {
                    show: true,
                },
                x: {
                    show: false,
                }
            },
            fill: {
                type: "gradient",
                gradient: {
                    type: "vertical",
                    shadeIntensity: 1,
                    inverseColors: !1,
                    opacityFrom: .28,
                    opacityTo: .05,
                    stops: [45, 100]
                }
            },
            responsive: [{
                breakpoint: 575,
                options: {
                    legend: {
                        offsetY: -30,
                    },
                },
            }]
        }

        /*
          /*
              ==============================
                  Statistics | Script
              ==============================
          */


        // Registered App Users

        var d_1C_5 = new ApexCharts(document.querySelector("#regAppChart"), regAppUser);
        d_1C_5.render()

        // Unregistered App Users

        var d_1C_6 = new ApexCharts(document.querySelector("#unregAppChart"), unregAppUser);
        d_1C_6.render()

        // Total Web Order
        var d_1C_7 = new ApexCharts(document.querySelector("#totWebChart"), regAppUser);
        d_1C_7.render()

        // Total Order Users
        var d_1C_8 = new ApexCharts(document.querySelector("#totOrderUser"), unregAppUser);
        d_1C_8.render()


        // User Subscriptions
        var chart = new ApexCharts(document.querySelector("#userSubsChart"), userSubscribtions);
        chart.render();

        // Revenue
        var chart1 = new ApexCharts(
            document.querySelector("#watchlistChart"),
            watchlist
        );

        chart1.render();

    }

    catch (e) {
        // statements
        console.log(e);
    }

}
arrSum = function (ar) {
    return ar.reduce(function (a, b) {
        return a + b
    }, 0);
}



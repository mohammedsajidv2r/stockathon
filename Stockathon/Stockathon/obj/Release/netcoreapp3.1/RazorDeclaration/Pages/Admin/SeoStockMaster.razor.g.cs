#pragma checksum "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\Admin\SeoStockMaster.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "139faf92724a989df3d6b7643b1aff743cb4c3a5"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace Stockathon.Pages.Admin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Utilities.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_BaseObjet;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_DA_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_DA;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Mgr;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Mgr_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Mgr_Interface.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_BaseObjet.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\Admin\SeoStockMaster.razor"
using SA_Utilities;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/seostock")]
    [Microsoft.AspNetCore.Components.RouteAttribute("/seostock/{param}")]
    public partial class SeoStockMaster : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 110 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\Admin\SeoStockMaster.razor"
       
    [Parameter] public string param { get; set; }
    ResultObject<List<Tbl_Seo_Stock>> resObject;
    Tbl_Seo_Stock _resModel;
    SAAdmin _SAAdmin;
    ElementReference SearchStockname;
    ResultObject<List<StockData>> profileobject = new ResultObject<List<StockData>>();


    ResultObject<List<StockData>> profileobjectData = new ResultObject<List<StockData>>();

    string increment = "";

    public string Page_TitleCount = "";
    public string Page_DescriptionCount = "";
    protected override void OnInitialized()
    {
        _SAAdmin = new SAAdmin(iConfig, httpContextAccessor, _IAdminMgr);
        _resModel = new Tbl_Seo_Stock();
        _resModel.Page_Title = "";
        _resModel.Page_Description = "";
        if (param != null)
        {
            param = param.Replace('_', '+');
            param = param.Replace('~', '/');
            param = Encryption.DecryptString(param, Encryption.passkey);
            if (param == "\0")
            {
                navigationmanager.NavigateTo("404");
            }
        }
        profileobject = _SAAdmin.stockWithId("selectSeoStock", "");
        resObject = _SAAdmin.viewSeoStock("viewStockSeoId", param);
        if (resObject.ResultData.Count > 0)
        {


            _resModel = resObject.ResultData.FirstOrDefault();
            titlecount = _resModel.Page_Title.Count();
            desccount = _resModel.Page_Description.Count();

            Page_TitleCount = resObject.ResultData.Select(x => x.Page_Title).FirstOrDefault();
            Page_DescriptionCount = resObject.ResultData.Select(x => x.Page_Description).FirstOrDefault();

            if (titlecount > 70)
            {
                titlecount = 100;
            }
            if (desccount > 140)
            {
                desccount = 100;
            }
            profileobjectData = _SAAdmin.stockWithId("getSymbolWithId", resObject.ResultData.Select(x => x.symbol).FirstOrDefault());
            if (profileobjectData.ResultData.Count > 0)
            {
                _resModel.page_content = profileobjectData.ResultData.Select(x => x.description).FirstOrDefault().ToString();
            }

            JSRuntime.InvokeVoidAsync("CSBindProgreeOnChange", titlecount);
            JSRuntime.InvokeVoidAsync("CSBindProgreeOnChangeDesc", _resModel.Page_Description.Count());
        }
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
    }

    protected override void OnAfterRender(bool firstRender)
    {
        JSRuntime.InvokeAsync<string>("setTitle", new object[] { "SEO" });
        if (firstRender)
        {
            JSRuntime.InvokeVoidAsync("CSBindProgreeOnChange", titlecount);
            JSRuntime.InvokeVoidAsync("CSBindProgreeOnChangeDesc", _resModel.Page_Description.Count());
            //JSRuntime.InvokeVoidAsync("ddl");

            JSRuntime.InvokeVoidAsync("CSBindingMultiSelect", "ddlFaq");
        }
        // JSRuntime.InvokeVoidAsync("AutoCompleteBinding",
        //profileobject.ResultData.Select(x => x.symbol)
        //);
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
    }


    int titlecount = 0;
    int desccount = 0;
    public void keychangetiltle(string key)
    {
        if (key.Count() > 70)
        {
            titlecount = 100;
        }
        else
        {
            titlecount = key.Count();
        }
        Page_TitleCount = key;
        JSRuntime.InvokeVoidAsync("CSBindProgreeOnChange", titlecount);
        StateHasChanged();
    }

    public void keychangeDescription(string key)
    {
        if (key.Count() > 140)
        {
            desccount = 100;
        }
        else
        {
            desccount = key.Count();
            desccount = desccount / 2;
        }
        Page_DescriptionCount = key;
        JSRuntime.InvokeVoidAsync("CSBindProgreeOnChangeDesc", key.Count());
        StateHasChanged();
    }

    async void OnConfirm()
    {
        await JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        ResultObject<string> res1 = new ResultObject<string>();
        var XMLData = XmlConversion.SerializeToXElement(_resModel);
        res1 = _SAAdmin.update("updateStockseo", "", "", XMLData.ToString(), "USP_Admin");
        if (res1.ResultMessage == "Success")
        {
            if (resObject.ResultData.Count > 0)
            {
                await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "SEO updated successfully.");
            }
            else
            {
                await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "SEO added successfully.");
            }
            navigationmanager.NavigateTo("stockseolist");
        }
        else
        {
            await JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
            await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Some Error Occured");
            resObject = new ResultObject<List<Tbl_Seo_Stock>>();
            _resModel = new Tbl_Seo_Stock();
            StateHasChanged();
            await JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
        }
    }

    protected void HandleInvalidSubmit()
    {
    }



    public void symboldataselect(string key)
    {
        profileobjectData = _SAAdmin.stockWithId("getSymbolWithId", key);
        if (profileobjectData.ResultData.Count > 0)
        {
            _resModel.page_content = profileobjectData.ResultData.Select(x => x.description).FirstOrDefault().ToString();
        }
        else
        {
            _resModel.page_content = "";
        }
        StateHasChanged();
    }

    public void symboldataselectddl(ChangeEventArgs e)
    {
        if (e.Value.ToString() == "Select Stocks")
        {
            _resModel.symbol = "";
        }
        else
        {
            _resModel.symbol = e.Value.ToString();
        }


        profileobjectData = _SAAdmin.stockWithId("getSymbolWithId", _resModel.symbol);
        if (profileobjectData.ResultData.Count > 0)
        {
            _resModel.page_content = profileobjectData.ResultData.Select(x => x.description).FirstOrDefault().ToString();
        }
        else
        {
            _resModel.page_content = "";
        }
        StateHasChanged();
    }



#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationmanager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAdminMgr _IAdminMgr { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpContextAccessor httpContextAccessor { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration iConfig { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
#pragma warning restore 1591

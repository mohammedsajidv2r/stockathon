#pragma checksum "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\Admin\FaqCms.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3edafe71a9a7acdb98a73dda0cd35729f402fe29"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace Stockathon.Pages.Admin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Utilities.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_BaseObjet;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_DA_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_DA;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Mgr;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Mgr_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_Mgr_Interface.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using SA_BaseObjet.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "C:\sajiddata\stockathon\Stockathon\Stockathon\_Imports.razor"
using Stockathon.Services;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/faqpage")]
    public partial class FaqCms : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 50 "C:\sajiddata\stockathon\Stockathon\Stockathon\Pages\Admin\FaqCms.razor"
       
    SAAdmin _SAAdmin;
    ResultObject<List<CmsPageData>> resObject;
    CmsPageData model;

    protected override void OnInitialized()
    {
        _SAAdmin = new SAAdmin(iConfig, httpContextAccessor, _IAdminMgr);
        model = new CmsPageData();
        resObject = _SAAdmin.cmsMasterreq("viewcmsreq", "Page");
        model = resObject.ResultData.FirstOrDefault();
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");

    }


    protected override void OnAfterRender(bool firstRender)
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
        JSRuntime.InvokeAsync<string>("setTitle", new object[] { "FAQ" });
    }

    protected void HandleInvalidSubmit()
    {
        //JSRuntime.InvokeVoidAsync("CSAlertMsg", "Info", "Please Fill Mandatory Field");
    }

    async void OnConfirm()
    {
        ResultObject<string> res1 = new ResultObject<string>();

        res1 = _SAAdmin.editCmsMaster("updatecms", "", model);
        if (res1.ResultMessage == "Success")
        {
            await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "Page updated successfully");
        }
        else if (res1.ResultMessage == "CMS Title Already Exists")
        {
            await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", res1.ResultMessage);
        }
        else
        {
            await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Some Error Occured");
        }
        resObject = _SAAdmin.cmsMasterreq("viewcmsreq", "Page");
        model = resObject.ResultData.FirstOrDefault();
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAdminMgr _IAdminMgr { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpContextAccessor httpContextAccessor { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration iConfig { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
#pragma warning restore 1591

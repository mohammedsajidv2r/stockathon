﻿using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using SA_BaseObjet;
using SA_BaseObjet.Admin;
using SA_BaseObjet.StocksApi;
using SA_DA_Interface.Admin;
using SA_Utilities;
using SA_Utilities.Enum;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SA_DA.Admin
{
   public class AdminDA : BaseConnection, IAdminDA
    {
        public AdminDA(IConfiguration config, IHttpContextAccessor httpContextAccessor) : base(config, httpContextAccessor)
        {

        }

        public ResultObject<Tbl_Admin_User> adminLoginAuth(string RequestType, Tbl_Admin_User data)
        {
            ResultObject<Tbl_Admin_User> resQuery = new ResultObject<Tbl_Admin_User>();
            try
            {
                string pass = "";
                //encrypt
                if (data.password != null)
                {
                    pass = Encryption.EncryptString(data.password, Encryption.passkey);
                }
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@email", data.email);
                parameters.Add("@password", pass);
                resQuery.ResultData = SqlMapper.Query<Tbl_Admin_User>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Tbl_Admin_User();
                    resQuery.ResultMessage = "Data Not Found.";
                    resQuery.Result = ResultType.Info;
                    //data.password = Encryption.DecryptString(data.password, Encryption.passkey);
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<Tbl_Stock_Symbol>> getStocks(string RequestType, string Id, string param, FilterData data)
        {
            ResultObject<List<Tbl_Stock_Symbol>> resStock = new ResultObject<List<Tbl_Stock_Symbol>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@PageCount", data.start);
                parameters.Add("@PageSize", data.length);
                parameters.Add("@PageSorting", data.sort);
                parameters.Add("@param", Id);
                resStock.ResultData = SqlMapper.Query<Tbl_Stock_Symbol>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resStock.ResultData == null || resStock.ResultData.Count <= 0)
                {
                    resStock.ResultData = new List<Tbl_Stock_Symbol>();
                    resStock.ResultMessage = "Data Not Found.";
                    resStock.Result = ResultType.Info;
                }
                else
                {
                    resStock.ResultMessage = "Success";
                    resStock.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getStocks", "AdminDA", 0, ex.Message.ToString());
                resStock.ResultMessage = ex.Message.ToString();
                resStock.Result = ResultType.Error;
            }
            return resStock;
        }

        public ResultObject<List<Usersdetails>> getUser(string RequestType, string Id, string param, FilterData data)
        {
            ResultObject<List<Usersdetails>> resStock = new ResultObject<List<Usersdetails>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@PageCount", data.start);
                parameters.Add("@PageSize", data.length);
                parameters.Add("@PageSorting", data.sort);
                parameters.Add("@param", Id);
                resStock.ResultData = SqlMapper.Query<Usersdetails>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resStock.ResultData == null || resStock.ResultData.Count <= 0)
                {
                    resStock.ResultData = new List<Usersdetails>();
                    resStock.ResultMessage = "Data Not Found.";
                    resStock.Result = ResultType.Info;
                }
                else
                {
                    resStock.ResultMessage = "Success";
                    resStock.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getUser", "AdminDA", 0, ex.Message.ToString());
                resStock.ResultMessage = ex.Message.ToString();
                resStock.Result = ResultType.Error;
            }
            return resStock;
        }

        public ResultObject<string> submitdetails(string RequestType, string Id, string param, string xml, string sp)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@Id", Id);
                parameters.Add("@XmlInput", xml);
                parameters.Add("@param", param);
                string _Result = SqlMapper.Query<string>(con, sp, param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "submitdetails", "AdminDA"+ sp, 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> updateProfile(string RequestType, string param1, Tbl_Admin_User data)
        {
            ResultObject<string> resQuery = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resQuery.ResultData = _Result.ToString();
                    resQuery.Result = ResultType.Success;
                    resQuery.ResultMessage = "Success";
                }
                else
                {
                    resQuery.ResultMessage = "Error Occured";
                    resQuery.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "updateProfile", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }


        public ResultObject<string> updatePassword(string RequestType, string param1, string param2, string param3)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                string _Result = "";
                if (RequestType == "passudpate")
                {
                    param1 = Encryption.EncryptString(param1, Encryption.passkey);
                    param2 = Encryption.EncryptString(param2, Encryption.passkey);
                    parameters.Add("@password", param1);
                    parameters.Add("@param", param2);
                    parameters.Add("@email", param3);
                    _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    if (_Result == "Success")
                    {
                        resPlan.ResultData = _Result.ToString();
                        resPlan.Result = ResultType.Success;
                        resPlan.ResultMessage = "Success";
                    }
                    else if (_Result == "The old password entered by you is incorrect")
                    {
                        resPlan.ResultData = _Result.ToString();
                        resPlan.Result = ResultType.Success;
                        resPlan.ResultMessage = _Result;
                    }
                    else
                    {
                        resPlan.ResultMessage = "Failure";
                        resPlan.Result = ResultType.Info;
                    }
                }
                else if (RequestType == "resetpassword")
                {
                    parameters.Add("@email", param1);
                    _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (_Result != "0")
                    {
                        resPlan.ResultData = _Result.ToString();
                        resPlan.Result = ResultType.Success;
                        resPlan.ResultMessage = "Success";
                    }
                    else
                    {
                        resPlan.ResultMessage = "Erron an occured";
                        resPlan.Result = ResultType.Info;
                    }
                }
                else
                {
                    param1 = Encryption.EncryptString(param1, Encryption.passkey);
                    parameters.Add("@password", param1);
                    parameters.Add("@param", param2);
                    parameters.Add("@id", param3);
                    _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    if (RequestType == "resetpasswordUpdate")
                    {
                        if (_Result == "Success")
                        {
                            resPlan.ResultData = _Result.ToString();
                            resPlan.Result = ResultType.Success;
                            resPlan.ResultMessage = "Success";
                        }
                        else
                        {
                            resPlan.ResultMessage = "some error Occured";
                            resPlan.Result = ResultType.Info;
                        }
                    }
                    else
                    {
                        if (_Result == "1")
                        {
                            resPlan.ResultData = _Result.ToString();
                            resPlan.Result = ResultType.Success;
                            resPlan.ResultMessage = "Success";
                        }
                        else if (_Result == "0")
                        {
                            resPlan.ResultData = _Result.ToString();
                            resPlan.Result = ResultType.Success;
                            resPlan.ResultMessage = "Success";
                        }
                        else
                        {
                            resPlan.ResultData = _Result.ToString();
                            resPlan.ResultMessage = "some error Occured";
                            resPlan.Result = ResultType.Info;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<Tbl_Admin_Intrinsic_Values> adminvVal(string RequestType)
        {
            ResultObject<Tbl_Admin_Intrinsic_Values> resQuery = new ResultObject<Tbl_Admin_Intrinsic_Values>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                resQuery.ResultData = SqlMapper.Query<Tbl_Admin_Intrinsic_Values>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Tbl_Admin_Intrinsic_Values();
                    resQuery.ResultMessage = "Data Not Found.";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "adminvVal", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> editCmsMaster(string RequestType, string param, CmsPageData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "CMS Title Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "editTagMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<CmsPageData>> cmsMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<CmsPageData>> resPlan = new ResultObject<List<CmsPageData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param1);
                resPlan.ResultData = SqlMapper.Query<CmsPageData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<CmsPageData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "cmsMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<ConfigurationData>> viewConfiguration(string RequestType, string param1, string param2)
        {
            ResultObject<List<ConfigurationData>> resPlan = new ResultObject<List<ConfigurationData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                resPlan.ResultData = SqlMapper.Query<ConfigurationData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<ConfigurationData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewConfiguration", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editConfiguration(string RequestType, string param, ConfigurationData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Erron an occured";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "editConfiguration", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<FaqData>> viewfaq(string RequestType, string param1, string param2)
        {
            ResultObject<List<FaqData>> resPlan = new ResultObject<List<FaqData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", param1);
                resPlan.ResultData = SqlMapper.Query<FaqData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<FaqData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewfaq", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editfaq(string RequestType, string param, FaqData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "Faq question Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Erron an occured";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "editfaq", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<tbl_PageSEO>> viewSeo(string RequestType, string param1, string param2)
        {
            ResultObject<List<tbl_PageSEO>> resPlan = new ResultObject<List<tbl_PageSEO>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", param1);
                resPlan.ResultData = SqlMapper.Query<tbl_PageSEO>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<tbl_PageSEO>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewSeo", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<StockData>> getStocksDetals(string RequestType, string Id, string param, FilterData data)
        {
            ResultObject<List<StockData>> resStock = new ResultObject<List<StockData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@PageCount", data.start);
                parameters.Add("@PageSize", data.length);
                parameters.Add("@PageSorting", data.sort);
                parameters.Add("@param", Id);
                resStock.ResultData = SqlMapper.Query<StockData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resStock.ResultData == null || resStock.ResultData.Count <= 0)
                {
                    resStock.ResultData = new List<StockData>();
                    resStock.ResultMessage = "Data Not Found.";
                    resStock.Result = ResultType.Info;
                }
                else
                {
                    resStock.ResultMessage = "Success";
                    resStock.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getStocksDetals", "AdminDA", 0, ex.Message.ToString());
                resStock.ResultMessage = ex.Message.ToString();
                resStock.Result = ResultType.Error;
            }
            return resStock;
        }

        public ResultObject<string> editStockProfile(string RequestType, string param, StockData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Erron an occured";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "editfaq", "editStockProfile", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<Tbl_Seo_Stock>> viewSeoStock(string RequestType, string param1, string param2)
        {
            ResultObject<List<Tbl_Seo_Stock>> resPlan = new ResultObject<List<Tbl_Seo_Stock>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", param1);
                resPlan.ResultData = SqlMapper.Query<Tbl_Seo_Stock>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<Tbl_Seo_Stock>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewSeoStock", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }
    }
}

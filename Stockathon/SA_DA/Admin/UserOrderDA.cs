﻿using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using SA_BaseObjet;
using SA_BaseObjet.Admin;
using SA_DA_Interface.Admin;
using SA_Utilities;
using SA_Utilities.Enum;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SA_DA.Admin
{
    public class UserOrderDA : BaseConnection, IUserOrderDA
    {
        public UserOrderDA(IConfiguration config, IHttpContextAccessor httpContextAccessor) : base(config, httpContextAccessor)
        {

        }

        public ResultObject<List<PlanOrderData>> viewPlanDetail(string RequestType, string param)
        {
            ResultObject<List<PlanOrderData>> resAppRev = new ResultObject<List<PlanOrderData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@userid", param);
                resAppRev.ResultData = SqlMapper.Query<PlanOrderData>(con, "USP_AdminUserOrder", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resAppRev.ResultData == null || resAppRev.ResultData.Count <= 0)
                {
                    resAppRev.ResultData = new List<PlanOrderData>();
                    resAppRev.ResultMessage = "Data Not Found.";
                    resAppRev.Result = ResultType.Info;
                }
                else
                {
                    resAppRev.ResultMessage = "Success";
                    resAppRev.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewPlanDetail", "UserOrderDA", 0, ex.Message.ToString());
                resAppRev.ResultMessage = ex.Message.ToString();
                resAppRev.Result = ResultType.Error;
            }
            return resAppRev;
        }

        public ResultObject<string> userPlanUpdate(string RequestType, List<PlanOrderData> data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_AdminUserOrder", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = _Result.ToString();
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "userPlanUpdate", "UserOrderDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<PlanData>> viewPlan(string RequestType, string param1, string param2)
        {
            ResultObject<List<PlanData>> resPlan = new ResultObject<List<PlanData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", param1);
                resPlan.ResultData = SqlMapper.Query<PlanData>(con, "USP_AdminUserOrder", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<PlanData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewPlan", "UserOrderDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> updatedetails(string RequestType, string Id, string xml, string param, string sp)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@Id", Id);
                parameters.Add("@XmlInput", xml);
                parameters.Add("@param", param);
                string _Result = SqlMapper.Query<string>(con, sp, param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "updatedetails", "UserOrderDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<OrderData>> viewOrderDetailFilter(string RequestType, FilterData data)
        {
            ResultObject<List<OrderData>> resUser = new ResultObject<List<OrderData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@PageCount", data.start);
                parameters.Add("@PageSize", data.length);
                parameters.Add("@PageSorting", data.sort);
                parameters.Add("@UserType", data.userType);
                resUser.ResultData = SqlMapper.Query<OrderData>(con, "USP_AdminUserOrder", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resUser.ResultData == null || resUser.ResultData.Count <= 0)
                {
                    resUser.ResultData = new List<OrderData>();
                    resUser.ResultMessage = "Data Not Found.";
                    resUser.Result = ResultType.Info;
                }
                else
                {
                    resUser.ResultMessage = "Success";
                    resUser.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "viewOrderDetailFilter", "UserOrderDA", 0, ex.Message.ToString());
                resUser.ResultMessage = ex.Message.ToString();
                resUser.Result = ResultType.Error;
            }
            return resUser;

        }
    }
}

﻿using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using SA_BaseObjet;
using SA_BaseObjet.StocksApi;
using SA_DA_Interface.StocksApi;
using SA_Utilities;
using SA_Utilities.Enum;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SA_DA.StocksApi
{
    public class StocksApiDA : BaseConnection, IStocksApiDA
    {
        public StocksApiDA(IConfiguration config, IHttpContextAccessor httpContextAccessor) : base(config, httpContextAccessor)
        {

        }
        public ResultObject<List<Tbl_Stock_Symbol>> getStocks(string RequestType, string Id, string param)
        {
            ResultObject<List<Tbl_Stock_Symbol>> resQuery = new ResultObject<List<Tbl_Stock_Symbol>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<Tbl_Stock_Symbol>(con, "USP_Stock", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData.Count <= 0)
                {
                    resQuery.ResultData = new List<Tbl_Stock_Symbol>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getStocks", "StocksApiDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }


        public ResultObject<string> submitdetails(string RequestType, string Id, string xml)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@Id", Id);
                parameters.Add("@XmlInput", xml);
                string _Result = SqlMapper.Query<string>(con, "USP_Stock", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "submitdetails", "StocksApiDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<Usersdetails> signup(string RequestType, Usersdetails data)
        {
            ResultObject<Usersdetails> resQuery = new ResultObject<Usersdetails>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                if (RequestType == "myProfile")
                {
                    parameters.Add("@id", data.user_id);
                }
                else
                {
                    parameters.Add("@XmlInput", XMLData);
                }

                resQuery.ResultData = SqlMapper.Query<Usersdetails>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Usersdetails();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "signup", "StocksApiDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<Financial> getFinancial(string RequestType, string Id, string param, string xml)
        {
            ResultObject<Financial> resFin = new ResultObject<Financial>();
            resFin.ResultData = new Financial();
            resFin.ResultData.pnl = new List<pnl>();
            resFin.ResultData.cashFlow = new List<cashFlow>();
            resFin.ResultData.balanceShhet = new List<balanceShhet>();
            resFin.ResultData.description = new description();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@Id", Id);
                parameters.Add("@param", param);
                using (var data = SqlMapper.QueryMultiple(con, "USP_Stock", param: parameters, commandType: CommandType.StoredProcedure))
                {
                    resFin.ResultData.pnl = data.Read<pnl>().ToList();
                    resFin.ResultData.balanceShhet = data.Read<balanceShhet>().ToList();
                    resFin.ResultData.cashFlow = data.Read<cashFlow>().ToList();
                    resFin.ResultData.description = data.Read<description>().FirstOrDefault();
                }
                if (resFin.ResultData.pnl.Count == 0 && resFin.ResultData.balanceShhet.Count == 0 && resFin.ResultData.cashFlow.Count == 0 && resFin.ResultData.description == null)
                {
                    resFin.ResultData = new Financial();
                    resFin.ResultMessage = "Data Not Found.";
                    resFin.Result = ResultType.Info;
                }
                else
                {
                    resFin.ResultMessage = "Success";
                    resFin.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getFinancial", "StocksApiDA", 0, ex.Message.ToString());
                resFin.ResultMessage = ex.Message.ToString();
                resFin.Result = ResultType.Error;
            }
            return resFin;
        }

        public ResultObject<List<dynamic>> getData(string RequestType, string Id, string param)
        {
            ResultObject<List<dynamic>> resQuery = new ResultObject<List<dynamic>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                parameters.Add("@param", param);
                resQuery.ResultData = SqlMapper.Query<dynamic>(con, "USP_Stock_OTHER", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData.Count <= 0)
                {
                    resQuery.ResultData = new List<dynamic>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getData", "StocksApiDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<DynamicList> getPeer(string RequestType, string Id, string param)
        {
            ResultObject<DynamicList> resFin = new ResultObject<DynamicList>();
            resFin.ResultData = new DynamicList();
            resFin.ResultData.dynamics = new List<dynamic>();
            resFin.ResultData.dynamics1 = new List<dynamic>();
            resFin.ResultData.dynamics2 = new List<dynamic>();
            resFin.ResultData.dynamics3 = new List<dynamic>();
            resFin.ResultData.dynamics4 = new List<dynamic>();
            resFin.ResultData.dynamics5 = new List<dynamic>();
            resFin.ResultData.dynamics6 = new List<dynamic>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@Id", Id);
                parameters.Add("@param", param);
                using (var data = SqlMapper.QueryMultiple(con, "USP_Stock", param: parameters, commandType: CommandType.StoredProcedure))
                {
                    resFin.ResultData.dynamics = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics1 = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics2 = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics3 = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics4 = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics5 = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics6 = data.Read<dynamic>().ToList();
                }
                if (resFin.ResultData.dynamics.Count == 0 && resFin.ResultData.dynamics1.Count == 0)
                {
                    resFin.ResultData = new DynamicList();
                    resFin.ResultMessage = "Data Not Found.";
                    resFin.Result = ResultType.Info;
                }
                else
                {
                    resFin.ResultMessage = "Success";
                    resFin.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getPeer", "StocksApiDA", 0, param+":"+ex.Message.ToString());
                resFin.ResultMessage = ex.Message.ToString();
                resFin.Result = ResultType.Error;
            }
            return resFin;
        }

        public ResultObject<string> updatedetails(string RequestType, string Id, string xml, string param)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                string sp = "";
                if (RequestType == "updatescreener")
                {
                    sp = "USP_Stock_Screener";
                }
                else
                {
                    sp = "USP_Stock";
                }

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@Id", Id);
                parameters.Add("@XmlInput", xml);
                parameters.Add("@param", param);
                string _Result = SqlMapper.Query<string>(con, sp, param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "updatedetails", "StocksApiDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> savedetails(string RequestType, string Id, string xml, string param, string sp)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@Id", Id);
                parameters.Add("@XmlInput", xml);
                parameters.Add("@param", param);
                string _Result = SqlMapper.Query<string>(con, sp, param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "updatedetails", "StocksApiDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<DynamicList> getDynamic5(string RequestType, string Id, string param, string sp)
        {
            ResultObject<DynamicList> resFin = new ResultObject<DynamicList>();
            resFin.ResultData = new DynamicList();
            resFin.ResultData.dynamics = new List<dynamic>();
            resFin.ResultData.dynamics1 = new List<dynamic>();
            resFin.ResultData.dynamics2 = new List<dynamic>();
            resFin.ResultData.dynamics3 = new List<dynamic>();
            resFin.ResultData.dynamics4 = new List<dynamic>();
            resFin.ResultData.dynamics5 = new List<dynamic>();
            resFin.ResultData.dynamics6 = new List<dynamic>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@Id", Id);
                parameters.Add("@param", param);
                using (var data = SqlMapper.QueryMultiple(con, sp, param: parameters, commandType: CommandType.StoredProcedure))
                {
                    resFin.ResultData.dynamics  = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics1 = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics2 = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics3 = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics4 = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics5 = data.Read<dynamic>().ToList();
                    resFin.ResultData.dynamics6 = data.Read<dynamic>().ToList();
                }
                if (resFin.ResultData.dynamics.Count == 0 && resFin.ResultData.dynamics1.Count == 0)
                {
                    resFin.ResultData = new DynamicList();
                    resFin.ResultMessage = "Data Not Found.";
                    resFin.Result = ResultType.Info;
                }
                else
                {
                    resFin.ResultMessage = "Success";
                    resFin.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                ErrorLogs._InsertLogs(RequestType, "getDynamic5", "StocksApiDA", 0, param + ":" + ex.Message.ToString());
                resFin.ResultMessage = ex.Message.ToString();
                resFin.Result = ResultType.Error;
            }
            return resFin;
        }
    }
}
